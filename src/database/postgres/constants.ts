export const POSTGRES_CONSTANTS = {
  extensionList: [
    'uuid-ossp',
    'pg_trgm',
    'postgis',
    'postgis_topology',
    'fuzzystrmatch',
    'postgis_tiger_geocoder',
    'postgis_raster',
    'postgis_sfcgal',
    'address_standardizer',
    'address_standardizer_data_us',
  ],
};
