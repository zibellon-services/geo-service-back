import { QueryTypes } from 'sequelize';
import { sequelizePGClient } from '../postgres-sequelize-client';

export type AddressIdAutocompleteSQL = {
  addressId: string;
}

//TODO - index. Отдельный поисковый словарь!
export async function addressIdListAutocompleteSQL(
  searchValue: string,
  lastWord: string
): Promise<AddressIdAutocompleteSQL[]> {
  return await sequelizePGClient.query<AddressIdAutocompleteSQL>(
    `SELECT * FROM
    (
      SELECT
        "adr"."id" AS "addressId",
        to_tsquery('${searchValue}') @@ to_tsVector(
          concat_ws(' ',
            "adr"."searchArea",
            "adr"."searchAreaDetails",
            "adr"."searchLocality",
            "adr"."searchLocalityDetails",
            "adr"."searchLocalityWay",
            "adr"."searchHouse",
            "adr"."searchHouseDetails1",
            "adr"."searchHouseDetails2",
            "adr"."searchPlace")) AS "addressMatch",
        to_tsquery('${lastWord}') @@ to_tsVector("ac"."name") AS "componentMatch"
      
      FROM "Addresses" AS "adr"
      JOIN "AddressComponents" as "ac" ON "ac"."id" = "adr"."targetComponentId"
      WHERE "geoDataId" IS NOT NULL
    ) AS "result"
    WHERE "result"."componentMatch" = true AND "result"."addressMatch" = true`,
    {
      type: QueryTypes.SELECT,
    }
  );
}
