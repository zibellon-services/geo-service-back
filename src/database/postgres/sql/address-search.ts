import { QueryTypes } from 'sequelize';
import { sequelizePGClient } from '../postgres-sequelize-client';

export type AddressIdSearchSQL = {
  addressId: string;
}

//TODO - index. Отдельный поисковый словарь!
export async function addressSearchSQL(searchValue: string, limit: number): Promise<AddressIdSearchSQL[]> {
  return await sequelizePGClient.query<AddressIdSearchSQL>(
    `SELECT * FROM
    (
      SELECT
        "adr"."id" AS "addressId",
        to_tsquery('${searchValue}') @@ to_tsVector(
          concat_ws(' ',
            "adr"."searchArea",
            "adr"."searchAreaDetails",
            "adr"."searchLocality",
            "adr"."searchLocalityDetails",
            "adr"."searchLocalityWay",
            "adr"."searchHouse",
            "adr"."searchHouseDetails1",
            "adr"."searchHouseDetails2",
            "adr"."searchPlace")) AS "addressMatch"
      
      FROM "Addresses" AS "adr"
      WHERE "geoDataId" IS NOT NULL
    ) AS "result"
    WHERE "result"."addressMatch" = true
    LIMIT ${limit}`,
    {
      type: QueryTypes.SELECT,
    }
  );
}

//город ... ЦЛица ... ттд
//Европейский

//id, similarityAddress, similarityPlace
