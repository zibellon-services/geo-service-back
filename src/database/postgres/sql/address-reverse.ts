import { QueryTypes } from 'sequelize';
import { CONSTANTS } from 'utils/constants';
import { sequelizePGClient } from '../postgres-sequelize-client';

export type AddressIdReverseSQL = {
  addressId: string;
}

//TODO - index. Отдельный поисковый словарь!
export async function addressIdReverseSQL(lat: number, lng: number): Promise<AddressIdReverseSQL[]> {
  return await sequelizePGClient.query<AddressIdReverseSQL>(
    `SELECT
      "adr"."id" AS "addressId"
    FROM "GeoData" AS "gd"
    JOIN "Addresses" AS "adr" ON "adr"."geoDataId" = "gd"."id" 
    WHERE ST_Intersects("polygon", ST_SetSRID(ST_MakePoint('${lat}', '${lng}'), '${CONSTANTS.SRID}')) = true
    ORDER BY ST_Area("polygon"), "adr"."placeId"
    LIMIT 1`,
    {
      type: QueryTypes.SELECT,
    }
  );
}

//город ... ЦЛица ... ттд
//Европейский

//id, similarityAddress, similarityPlace
