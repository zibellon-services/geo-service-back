import { AddressComponent } from './final/address-component.model';
import { Address } from './final/address.model';
import { GeoData } from './final/geo-data.model';

export const models = [AddressComponent, Address, GeoData];
