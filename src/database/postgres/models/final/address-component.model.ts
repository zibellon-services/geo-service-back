import { literal } from 'sequelize';
import { BelongsTo, Column, DataType, ForeignKey, HasMany, HasOne, Model, Table } from 'sequelize-typescript';
import { AddressComponentZone } from 'utils/constants';
import { AddressComponentTypeEN } from 'utils/constants-en';
import { Address } from './address.model';
import { GeoData } from './geo-data.model';

@Table({
  timestamps: true,
})
export class AddressComponent extends Model {
  @Column({
    defaultValue: literal(`uuid_generate_v4()`),
    type: DataType.UUID,
    primaryKey: true,
  })
  public id!: string;

  @Column({
    type: DataType.STRING,
  })
  public name!: string;

  @Column({
    type: DataType.ENUM({ values: Object.values(AddressComponentZone) }),
  })
  public zone!: AddressComponentZone;

  @Column({
    type: DataType.ENUM({ values: Object.values(AddressComponentTypeEN) }),
  })
  public type!: AddressComponentTypeEN;

  //Parent - Child структура
  //Родительский компонент
  @ForeignKey(() => AddressComponent)
  public parentId!: string;

  @BelongsTo(() => AddressComponent, {
    foreignKey: 'parentId',
    onDelete: 'CASCADE',
  })
  public parent!: AddressComponent;

  //Все дочерние компоненты
  @HasMany(() => AddressComponent, {
    foreignKey: 'parentId',
  })
  public childComponentList!: AddressComponent;

  //Координаты адреса (Точка + полигон)
  @HasOne(() => GeoData, {
    foreignKey: 'componentId',
    onDelete: 'CASCADE',
  })
  public geoData!: GeoData;

  @HasOne(() => Address, {
    foreignKey: 'targetComponentId',
    onDelete: 'CASCADE',
  })
  public address!: Address;
}
