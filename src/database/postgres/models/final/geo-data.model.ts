import { literal } from 'sequelize';
import { BelongsTo, Column, DataType, ForeignKey, Model, Table } from 'sequelize-typescript';
import { CONSTANTS } from 'utils/constants';
import { AddressComponent } from './address-component.model';

@Table({
  timestamps: true,
})
export class GeoData extends Model {
  @Column({
    defaultValue: literal(`uuid_generate_v4()`),
    type: DataType.UUID,
    primaryKey: true,
  })
  public id!: string;

  @Column({
    type: DataType.GEOGRAPHY('POINT', CONSTANTS.SRID),
  })
  public point!: any;

  @Column({
    type: DataType.GEOGRAPHY('POLYGON', CONSTANTS.SRID),
  })
  public polygon!: any;

  @ForeignKey(() => AddressComponent)
  public componentId!: string;

  @BelongsTo(() => AddressComponent, {
    foreignKey: 'componentId',
    onDelete: 'CASCADE',
  })
  public component!: AddressComponent;
}
