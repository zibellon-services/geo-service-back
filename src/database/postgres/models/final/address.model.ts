import { literal } from 'sequelize';
import { BelongsTo, Column, DataType, ForeignKey, Model, Table } from 'sequelize-typescript';
import { AddressType } from 'utils/constants';
import { mapDBAddressDisplayDescription, mapDBAddressDisplayName } from 'utils/utils-map-db-address';
import { AddressComponent } from './address-component.model';
import { GeoData } from './geo-data.model';

@Table({
  timestamps: true,
})
export class Address extends Model {
  @Column({
    defaultValue: literal(`uuid_generate_v4()`),
    type: DataType.UUID,
    primaryKey: true,
  })
  public id!: string;

  @Column({
    type: DataType.ENUM({ values: Object.values(AddressType) }),
    defaultValue: AddressType.ADDRESS,
  })
  public type!: AddressType;

  //Поля, которые отвечают за поиск
  @Column({
    type: DataType.STRING,
  })
  public searchArea!: string;

  @Column({
    type: DataType.STRING,
  })
  public searchAreaDetails!: string;

  @Column({
    type: DataType.STRING,
  })
  public searchLocality!: string;

  @Column({
    type: DataType.STRING,
  })
  public searchLocalityDetails!: string;

  @Column({
    type: DataType.STRING,
  })
  public searchLocalityWay!: string;

  @Column({
    type: DataType.STRING,
  })
  public searchHouse!: string;

  @Column({
    type: DataType.STRING,
  })
  public searchHouseDetails1!: string;

  @Column({
    type: DataType.STRING,
  })
  public searchHouseDetails2!: string;

  @Column({
    type: DataType.STRING,
  })
  public searchPlace!: string;

  //TODO - УДАЛИТЬ виртуальные поля
  //Виртуальные поля, которые собираются в момент отдачи сущности на фронт
  @Column(DataType.VIRTUAL)
  get displayName() {
    return mapDBAddressDisplayName(this);
  }

  @Column(DataType.VIRTUAL)
  get displayDescription() {
    return mapDBAddressDisplayDescription(this);
  }

  //Поля, для Сущностей Компонентов
  @ForeignKey(() => AddressComponent)
  public areaId!: string;

  @BelongsTo(() => AddressComponent, { as: 'area', foreignKey: 'areaId' })
  public area!: AddressComponent;

  @ForeignKey(() => AddressComponent)
  public areaDetailsId!: string;

  @BelongsTo(() => AddressComponent, { as: 'areaDetails', foreignKey: 'areaDetailsId' })
  public areaDetails!: AddressComponent;

  @ForeignKey(() => AddressComponent)
  public localityId!: string;

  @BelongsTo(() => AddressComponent, { as: 'locality', foreignKey: 'localityId' })
  public locality!: AddressComponent;

  @ForeignKey(() => AddressComponent)
  public localityDetailsId!: string;

  @BelongsTo(() => AddressComponent, { as: 'localityDetails', foreignKey: 'localityDetailsId' })
  public localityDetails!: AddressComponent;

  @ForeignKey(() => AddressComponent)
  public localityWayId!: string;

  @BelongsTo(() => AddressComponent, { as: 'localityWay', foreignKey: 'localityWayId' })
  public localityWay!: AddressComponent;

  @ForeignKey(() => AddressComponent)
  public houseId!: string;

  @BelongsTo(() => AddressComponent, { as: 'house', foreignKey: 'houseId' })
  public house!: AddressComponent;

  @ForeignKey(() => AddressComponent)
  public houseDetails1Id!: string;

  @BelongsTo(() => AddressComponent, { as: 'houseDetails1', foreignKey: 'houseDetails1Id' })
  public houseDetails1!: AddressComponent;

  @ForeignKey(() => AddressComponent)
  public houseDetails2Id!: string;

  @BelongsTo(() => AddressComponent, { as: 'houseDetails2', foreignKey: 'houseDetails2Id' })
  public houseDetails2!: AddressComponent;

  @ForeignKey(() => AddressComponent)
  public placeId!: string;

  @BelongsTo(() => AddressComponent, { as: 'place', foreignKey: 'placeId' })
  public place!: AddressComponent;

  @ForeignKey(() => AddressComponent)
  public targetComponentId!: string;

  @BelongsTo(() => AddressComponent, { as: 'targetComponent', foreignKey: 'targetComponentId', onDelete: 'CASCADE' })
  public targetComponent!: AddressComponent;

  @ForeignKey(() => GeoData)
  public geoDataId!: string;

  @BelongsTo(() => GeoData, {
    foreignKey: 'geoDataId',
  })
  public geoData!: GeoData;
}
