import { AddressComponent } from 'database/postgres/models/final/address-component.model';
import { Address } from 'database/postgres/models/final/address.model';
import { ResAPIV1Address, ResAPIV1Component } from './classes';
import { AddressType, OSM_TYPE } from './constants';
import { AddressComponentTypeRU } from './constants-ru';
import { enumKeyToVal } from './utils-map-enums';
import { mapComponentListExtras, mapComponentListToDisplayInfo } from './utils-mapper';

export function mapDBAddressToAPIV1Response(address: Address): ResAPIV1Address {
  const componentList = mapComponentListToAPIV1Response(address);
  return {
    osmType: address.type === AddressType.PLACE ? OSM_TYPE.NODE : OSM_TYPE.WAY,
    ...mapComponentListToDisplayInfo(componentList),
    ...mapComponentListExtras(componentList),
    coordinates: {
      lat: address.geoData.point.coordinates[0],
      lng: address.geoData.point.coordinates[1],
    },
    componentList,
  };
}

function mapComponentListToAPIV1Response(address: Address): ResAPIV1Component[] {
  let arr: ResAPIV1Component[] = [];

  if (address.area) {
    arr.push(mapDBAddressComponentToAPIV1Response(address.area));
  }
  if (address.areaDetails) {
    arr.push(mapDBAddressComponentToAPIV1Response(address.areaDetails));
  }
  if (address.locality) {
    arr.push(mapDBAddressComponentToAPIV1Response(address.locality));
  }
  if (address.localityDetails) {
    arr.push(mapDBAddressComponentToAPIV1Response(address.localityDetails));
  }
  if (address.localityWay) {
    arr.push(mapDBAddressComponentToAPIV1Response(address.localityWay));
  }
  if (address.house) {
    arr.push(mapDBAddressComponentToAPIV1Response(address.house));
  }
  if (address.houseDetails1) {
    arr.push(mapDBAddressComponentToAPIV1Response(address.houseDetails1));
  }
  if (address.houseDetails2) {
    arr.push(mapDBAddressComponentToAPIV1Response(address.houseDetails2));
  }
  if (address.place) {
    arr.push(mapDBAddressComponentToAPIV1Response(address.place));
  }
  return arr;
}

export function mapDBAddressComponentToAPIV1Response(dbAddressComponent: AddressComponent): ResAPIV1Component {
  const keyString = String(dbAddressComponent.type);
  return {
    zone: dbAddressComponent.zone,
    type: enumKeyToVal(keyString, AddressComponentTypeRU),
    typeKey: keyString,
    name: dbAddressComponent.name,
  };
}

export function mapDBAddressDisplayName(address: Address) {
  //Если есть место - то мы сразу возвращем его название
  if (address.place) {
    return address.place.name;
  }
  return mapDBAddressDisplayDescription(address);
}

export function mapDBAddressDisplayDescription(address: Address): string {
  let arr = [];

  if (address.area) {
    arr.push(mapDBComponentTypeENandNameToRU(String(address.area.type), address.area.name));
  }
  if (address.areaDetails) {
    arr.push(mapDBComponentTypeENandNameToRU(String(address.areaDetails.type), address.areaDetails.name));
  }
  if (address.locality) {
    arr.push(mapDBComponentTypeENandNameToRU(String(address.locality.type), address.locality.name));
  }
  if (address.localityDetails) {
    arr.push(mapDBComponentTypeENandNameToRU(String(address.localityDetails.type), address.localityDetails.name));
  }
  if (address.localityWay) {
    arr.push(mapDBComponentTypeENandNameToRU(String(address.localityWay.type), address.localityWay.name));
  }
  if (address.house) {
    arr.push(mapDBComponentTypeENandNameToRU(String(address.house.type), address.house.name));
  }
  if (address.houseDetails1) {
    arr.push(mapDBComponentTypeENandNameToRU(String(address.houseDetails1.type), address.houseDetails1.name));
  }
  if (address.houseDetails2) {
    arr.push(mapDBComponentTypeENandNameToRU(String(address.houseDetails2.type), address.houseDetails2.name));
  }
  return arr.join(', ');
}

export function mapDBComponentTypeENandNameToRU(componentTypeEN: string, componentName: string) {
  const componentTypeRU = enumKeyToVal(componentTypeEN, AddressComponentTypeRU).toLocaleLowerCase();
  return componentTypeRU + ' ' + componentName;
}
