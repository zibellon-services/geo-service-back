import { compareTwoStrings } from 'string-similarity';
import { PriorityQueryParams, ResAPIV1Address } from './classes';
import { AddressComponentZone, OSM_TYPE } from './constants';

export function sortResAPIV1AddressList(
  addressList: ResAPIV1Address[],
  searchValue: string,
  priorityParams: PriorityQueryParams
): ResAPIV1Address[] {
  if (priorityIsAble(priorityParams)) {
    const arrWithPriority: Array<{ address: ResAPIV1Address; weight: number }> = [];
    const arrNOPriority: Array<{ address: ResAPIV1Address; weight: number }> = [];

    for (const el of addressList) {
      const priorityIndex = getPriorityIndexForResAPIV1Address(el, priorityParams);
      const compareSearchIndex = compareTwoStrings(el.shortDescription, searchValue);
      if (priorityIndex > 0) {
        arrWithPriority.push({
          address: el,
          weight: priorityIndex + compareSearchIndex,
        });
      } else {
        arrNOPriority.push({
          address: el,
          weight: compareSearchIndex,
        });
      }
    }

    arrWithPriority.sort((a, b) => {
      return b.weight - a.weight;
    });

    arrNOPriority.sort((a, b) => {
      return b.weight - a.weight;
    });

    return [...arrWithPriority, ...arrNOPriority].map((el) => el.address);
  } else {
    const wayArr: ResAPIV1Address[] = [];
    const nodeArr: ResAPIV1Address[] = [];
    const relationArr: ResAPIV1Address[] = [];

    //Сначала сортируем
    addressList.sort((a, b) => {
      return compareTwoStrings(searchValue, b.shortDescription) - compareTwoStrings(searchValue, a.shortDescription);
    });

    for (const el of addressList) {
      if (el.osmType === OSM_TYPE.WAY) {
        wayArr.push(el);
      } else if (el.osmType === OSM_TYPE.NODE) {
        nodeArr.push(el);
      } else {
        relationArr.push(el);
      }
    }

    return [...wayArr, ...nodeArr, ...relationArr];
  }
}

export function priorityIsAble(priorityParams: PriorityQueryParams) {
  return priorityParams.areaPriority || priorityParams.localityPriority || priorityParams.localityWayPriority;
}

function getPriorityIndexForResAPIV1Address(address: ResAPIV1Address, priorityParams: PriorityQueryParams) {
  let index = 0;

  for (const el of address.componentList) {
    if (el.zone === AddressComponentZone.AREA) {
      if (priorityParams.areaPriority && priorityParams.areaPriority.length > 0) {
        index += compareTwoStrings(el.name, priorityParams.areaPriority);
      }
    } else if (el.zone === AddressComponentZone.LOCALITY) {
      if (priorityParams.localityPriority && priorityParams.localityPriority.length > 0) {
        index += compareTwoStrings(el.name, priorityParams.localityPriority);
      }
    } else if (el.zone === AddressComponentZone.LOCALITY_WAY) {
      if (priorityParams.localityWayPriority && priorityParams.localityWayPriority.length > 0) {
        index += compareTwoStrings(el.name, priorityParams.localityWayPriority);
      }
    }
  }

  return index;
}

export function sortOSMResult(osmArr: any[], searchValue: string) {
  const wayArr: any[] = [];
  const nodeArr: any[] = [];
  const relationArr: any[] = [];

  //Сначала сортируем
  osmArr.sort((a, b) => {
    return compareTwoStrings(searchValue, b.display_name) - compareTwoStrings(searchValue, a.display_name);
  });

  for (const el of osmArr) {
    if (el.osm_type === OSM_TYPE.WAY) {
      wayArr.push(el);
    } else if (el.osm_type === OSM_TYPE.NODE) {
      nodeArr.push(el);
    } else {
      relationArr.push(el);
    }
  }

  return [...wayArr, ...nodeArr, ...relationArr];
}
