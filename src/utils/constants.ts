export const CONSTANTS = {
  LOCAL_URL: `http://localhost`, //Адрес локалХост
  SWAGGER_UI_DIST_PATH: `swagger-ui-dist`, //маска для swagger docs
  SWAGGER_DOCS: `/api/swagger/docs`, //маска для swagger docs
  SWAGGER_JSON: `/api/swagger/json`, //маска для swagger SHEMA (get-json)
  MEDIA_FOLDER: `/media`, //Папка, где хранятся все документы

  SRID: 4326,
  ADDRESS_AUTOCOMPLETE_LIMIT: 50,
  ADDRESS_SEARCH_LIMIT: 50,

  AREA_REGEX: /(\s|^)(округ|район|область|край|республика|регион|городской округ|муниципальный округ)(\s|$)/i,
  LOCALITY_REGEX:
    /(\s|^)(округ|город|деревня|село|поселок|посёлок|сельское поселение|коттеджный посёлок|коттеджный поселок|СНТ|КИЗ|СОТ|СТ|СОНТ|ТСО|ТИЗ)(\s|$)/i,
  LOCALITY_WAY_REGEX: /(\s|^)(переулок|проезд|улица|шоссе|площадь|парк|проспект|бульвар|набережная|линия)(\s|$)/i,
};

export enum AddressComponentZone {
  AREA = 'AREA', //округ
  AREA_DETAILS = 'AREA_DETAILS', //Некоторое уточнение для AREA
  LOCALITY = 'LOCALITY', // город деревня село снт
  LOCALITY_DETAILS = 'LOCALITY_DETAILS', // УТОЧНЕНИЕ для: город деревня село снт
  LOCALITY_WAY = 'LOCALITY_WAY', //улица бульвар площадь
  HOUSE = 'HOUSE', //дом
  HOUSE_DETAILS_1 = 'HOUSE_DETAILS_1', //уточнения
  HOUSE_DETAILS_2 = 'HOUSE_DETAILS_2',
  PLACE = 'PLACE', //МЕСТО
}

//Типы адреса
export enum AddressType {
  PLACE = 'PLACE',
  ADDRESS = 'ADDRESS',
}

export const PHOTON_LANG_Q = {
  lang: 'en',
};

export const OSM_TYPE = {
  WAY: 'way',
  W: 'W',
  NODE: 'node',
  N: 'N',
  RELATION: 'relation',
  R: 'R',
};

const k = ['AREA', 'LOCALITY', 'LOCALITY_WAY', 'HOUSE', 'HOUSE_DETAILS_1', 'HOUSE_DETAILS_2'];

//Что прийдет
const e = [
  { field: 'LOCALITY' },
  { field: 'HOUSE' },
  { field: 'AREA' },
  { field: 'HOUSE_DETAILS_1' },
  { field: 'LOCALITY_WAY' },
];

//Что должно быть в результате
const w = [
  { field: 'AREA' },
  { field: 'LOCALITY' },
  { field: 'LOCALITY_WAY' },
  { field: 'HOUSE' },
  { field: 'HOUSE_DETAILS_1' },
];

const TestDataParse = [
  '25E-4/3к1с5-1',
  '25кк1-7с5-1',
  '11/2 с2/1',
  '5К',
  '6-2',
  '11 к2',
  '16с3',
  '32 к2А',
  '1 к1с12',
  '25',
];
//5K 11k2

//Правила парсинга

//Идея 1
// 25E-4/3к1с5-1 -> ['25Е-4/3', 'к1', 'с5-1']

// /(к|корп)([0-9-/]+)/
//
// /(к|корп\.?)([0-9-/]+)/

//extract корпуса - /к([0-9-/]+)/ -> вырезаем
//extract корпуса - /с([0-9-/]+)/ -> вырезаем
//остаток - 25E-4/3 - это и есть дом

//Московский проспект 25 к1 с5

//Список данныз для улиц

const testStreetParse = [
  'улица Димитрова',
  'Железнодорожная улица',
  '1-й проезд Суфтина',
  'улица 5-я Линия',
  'Соловецкая улица, 1-й переулок',
  'Талажское шоссе',
  'улица Аэропорт Архангельск',
];
