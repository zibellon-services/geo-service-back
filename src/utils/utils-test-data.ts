import { AddressComponent } from 'database/postgres/models/final/address-component.model';
import { Address } from 'database/postgres/models/final/address.model';
import { GeoData } from 'database/postgres/models/final/geo-data.model';
import { AddressComponentZone } from './constants';
import {
  AddressComponentTypeAreaEN,
  AddressComponentTypeHouseDetailEN,
  AddressComponentTypeHouseEN,
  AddressComponentTypeLocalityEN,
  AddressComponentTypeLocalityWayEN,
} from './constants-en';

export async function testData() {
  const geographyDataObl = {
    type: 'Polygon',
    coordinates: [
      [
        [64.73224327099874, 40.57382387552042],
        [64.47190400467166, 40.03549379739542],
        [64.2293716882555, 40.94186586770792],
        [64.60650776873906, 41.31540102395792],
        [64.73224327099874, 40.57382387552042],
      ],
    ],
  };
  const geographyDataArch = {
    type: 'Polygon',
    coordinates: [
      [
        [64.61484310821496, 40.566371684840995],
        [64.51132423160354, 40.45788169460662],
        [64.47761804858708, 40.60345054226287],
        [64.58332772980128, 40.68310142116912],
        [64.61484310821496, 40.566371684840995],
      ],
    ],
  };
  const coordForAll = {
    type: 'Point',
    coordinates: [64.54439, 40.52618],
  };
  const geographyDataStreet = {
    type: 'Polygon',
    coordinates: [
      [
        [64.54598203011592, 40.52942261108677],
        [64.5439161746121, 40.528392642825054],
        [64.54504134746017, 40.51706299194615],
        [64.54699645544147, 40.51805004486363],
        [64.54598203011592, 40.52942261108677],
      ],
    ],
  };
  const geographyDataHouse = {
    type: 'Polygon',
    coordinates: [
      [
        [64.54448335424873, 40.52600542327881],
        [64.54428737060198, 40.525908863754275],
        [64.54424817370364, 40.526166355819704],
        [64.54444415763206, 40.5262736441803],
        [64.54448335424873, 40.52600542327881],
      ],
    ],
  };
  const geographyDataHouseSad = {
    type: 'Polygon',
    coordinates: [
      [
        [64.54438471724262, 40.52505380573449],
        [64.54443083100398, 40.525091356660695],
        [64.54444005374688, 40.52500552597222],
        [64.54456456047095, 40.52504307689843],
        [64.54455994912094, 40.525102085496755],
        [64.54459222855469, 40.52518791618523],
        [64.54464987030563, 40.52523083152947],
        [64.54461067392823, 40.525633162881704],
        [64.54433629770931, 40.52546150150475],
      ],
    ],
  };
  const geographyDataDetails = {
    type: 'Polygon',
    coordinates: [
      [
        [64.54476284778409, 40.52521473827538],
        [64.54492193772553, 40.52527374687371],
        [64.54485046264922, 40.525863832856984],
        [64.54468676096226, 40.52578873100457],
        [64.54476284778409, 40.52521473827538],
      ],
    ],
  };
  const geographyDataStreet1 = {
    type: 'Polygon',
    coordinates: [
      [
        [64.54254138108124, 40.54930158297393],
        [64.54551993214876, 40.55195186709933],
        [64.5458473245385, 40.55307839488559],
        [64.54459767893664, 40.554676991458464],
        [64.5420625887342, 40.55222674674489],
        [64.54254138108124, 40.54930158297393],
      ],
    ],
  };
  const geographyDataHouse1 = {
    type: 'Polygon',
    coordinates: [
      [
        [64.54216967467391, 40.5520433709595],
        [64.5424210141382, 40.55227940535281],
        [64.54247174292348, 40.551968269107086],
        [64.54221348630682, 40.55177515005801],
        [64.54216967467391, 40.5520433709595],
      ],
    ],
  };
  const geographyDataHouseDetail1 = {
    type: 'Polygon',
    coordinates: [
      [
        [64.54269202052946, 40.550660336680195],
        [64.54279808864636, 40.55075689620473],
        [64.5426943263625, 40.55131479567983],
        [64.54260209288987, 40.55122360057332],
        [64.54269202052946, 40.550660336680195],
      ],
    ],
  };
  const geoDataObl = await GeoData.create({
    point: coordForAll,
    polygon: geographyDataObl,
  });
  const geoDataArch = await GeoData.create({
    point: coordForAll,
    polygon: geographyDataArch,
  });
  const geoDataStreet = await GeoData.create({
    point: coordForAll,
    polygon: geographyDataStreet,
  });
  const geoDataHouse = await GeoData.create({
    point: coordForAll,
    polygon: geographyDataHouse,
  });
  const geoDataDetails = await GeoData.create({
    point: coordForAll,
    polygon: geographyDataDetails,
  });
  const geoDataHouseSad = await GeoData.create({
    point: coordForAll,
    polygon: geographyDataHouseSad,
  });

  const geoDataStreet1 = await GeoData.create({
    point: coordForAll,
    polygon: geographyDataStreet1,
  });
  const geoDataHouse1 = await GeoData.create({
    point: coordForAll,
    polygon: geographyDataHouse1,
  });
  const geoDataHouseDeta1 = await GeoData.create({
    point: coordForAll,
    polygon: geographyDataHouseDetail1,
  });

  const addressComponentObl = await AddressComponent.create({
    name: 'Архангельская',
    geoDataId: geoDataObl.id,
    zone: AddressComponentZone.AREA,
    type: AddressComponentTypeAreaEN.REGION,
  });
  const addressComponentArch = await AddressComponent.create({
    name: 'Архангельск',
    geoDataId: geoDataArch.id,
    zone: AddressComponentZone.LOCALITY,
    type: AddressComponentTypeLocalityEN.CITY,
    parentId: addressComponentObl.id,
  });
  const addressComponentStreet = await AddressComponent.create({
    name: 'Карла Маркса',
    geoDataId: geoDataStreet.id,
    zone: AddressComponentZone.LOCALITY_WAY,
    type: AddressComponentTypeLocalityWayEN.STREET,
    parentId: addressComponentArch.id,
  });

  const addressComponentStreet1 = await AddressComponent.create({
    name: 'Суфтина',
    geoDataId: geoDataStreet1.id,
    zone: AddressComponentZone.LOCALITY_WAY,
    type: AddressComponentTypeLocalityWayEN.STREET,
    parentId: addressComponentArch.id,
  });
  const addressComponentHouse1 = await AddressComponent.create({
    name: '8',
    geoDataId: geoDataHouse1.id,
    zone: AddressComponentZone.HOUSE,
    type: AddressComponentTypeHouseEN.HOUSE,
    parentId: addressComponentStreet1.id,
  });
  const addressComponentDetails1 = await AddressComponent.create({
    name: '1',
    geoDataId: geoDataHouseDeta1.id,
    zone: AddressComponentZone.HOUSE_DETAILS_1,
    type: AddressComponentTypeHouseDetailEN.CORPUS,
    parentId: addressComponentHouse1.id,
  });

  const addressComponentHouse = await AddressComponent.create({
    name: '31',
    geoDataId: geoDataHouse.id,
    zone: AddressComponentZone.HOUSE,
    type: AddressComponentTypeHouseEN.HOUSE,
    parentId: addressComponentStreet.id,
  });
  const addressComponentHouseSad = await AddressComponent.create({
    name: '29',
    geoDataId: geoDataHouseSad.id,
    zone: AddressComponentZone.HOUSE,
    type: AddressComponentTypeHouseEN.HOUSE,
    parentId: addressComponentStreet.id,
  });
  const addressComponentDetails = await AddressComponent.create({
    name: '1',
    geoDataId: geoDataDetails.id,
    zone: AddressComponentZone.HOUSE_DETAILS_1,
    type: AddressComponentTypeHouseDetailEN.CORPUS,
    parentId: addressComponentHouseSad.id,
  });
}

export async function testShowData() {
  const addressList = await Address.findAll();
  console.log(JSON.stringify(addressList, null, 2));
  return addressList;
}

export async function testGeoData() {
  //Создаем 1 элемент
  const geoData1 = await GeoData.create({
    point: {
      type: 'Point',
      coordinates: [31.12345, 41.12345],
    },
    polygon: {
      type: 'Polygon',
      coordinates: [
        [
          [30.12345, 40.12345],
          [30.12345, 42.23456],
          [32.23456, 42.23456],
          [32.23456, 40.12345],
          [30.12345, 40.12345],
        ],
      ],
    },
  });

  console.log('CREATED_1 =', geoData1);

  const geoData2 = await GeoData.create({
    point: {
      type: 'Point',
      coordinates: [31.12345, 41.12345],
    },
    polygon: {
      type: 'Polygon',
      coordinates: [
        [
          [30.12345, 40.12345],
          [30.12345, 42.23456],
          [32.23456, 42.23456],
          [32.23456, 40.12345],
          [30.12345, 40.12345],
        ],
      ],
    },
  });

  console.log('CREATED_1 =', geoData2);

  //Пытаемся найти эту точку в БД

  // const geoDataFind = await GeoData.findOne({
  //   where: {
  //     point: Sequelize.fn('ST_GeomFromGeoJSON', `{"type":"Point","coordinates":[31.12345, 41.12345]}`),
  //     polygon: Sequelize.fn(
  //       'ST_GeomFromGeoJSON',
  //       `{"type":"Polygon","coordinates":[[[30.12345, 40.12345],[30.12345, 42.23456],[32.23456, 42.23456],[32.23456, 40.12345],[30.12345, 40.12345]]]}`
  //     ),
  //   },
  // });

  // console.log('FIND =', geoDataFind);
}
