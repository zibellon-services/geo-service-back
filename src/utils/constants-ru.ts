export enum AddressComponentTypeAreaRU {
  COUNTY = 'округ',
  CITY_COUNTY = 'городской округ',
  AREA = 'район',
  REGION = 'область',
  EDGE = 'край',
  REPUBLIC = 'республика',
}

export enum AddressComponentTypeLocalityRU {
  CITY = 'город',
  VILLAGE = 'деревня',
  SUBURB = 'микрорайон',
  HAMLET = 'поселок',
  SELO = 'село',
  COUNTY = 'округ',
  RURAL_SETTELMENT = 'сельское поселение',
  COTTAGE_SETTELMENT = 'коттеджный поселок',
  ST = 'СТ',
  SNT = 'СНТ',
  SOT = 'СОТ',
  SONT = 'СОНТ',
  KIZ = 'КИЗ',
  TSO = 'ТСО',
  TIZ = 'ТИЗ',
}

export enum AddressComponentTypeLocalityWayRU {
  STREET = 'улица',
  LINE = 'линия',
  ROADWAY = 'шоссе',
  SQUARE = 'площадь',
  PARK = 'парк',
  AVENUE = 'проспект',
  TRAVEL = 'проезд',
  LANE = 'переулок',
  BOULEVARD = 'бульвар',
  EMBANKMENT = 'набережная',
}

export enum AddressComponentTypeHouseRU {
  HOUSE = 'дом',
}

export enum AddressComponentTypeHouseDetailRU {
  STRUCTURE = 'строение',
  CORPUS = 'корпус',
  ANNEX = 'пристройка',
}

export enum AddressComponentTypePlaceRU {
  SIMPLE_PLACE = 'просто место',
  AIRPORT = 'аэропорт',
  SHOP = 'магазин',
  SPORT = 'спорт',
  TOURISM = 'туризм',
  HISTORIC = 'историческое',
  RAILWAY = 'Ж/Д',
  AMENITY = 'удобства',
  JOYANDREST = 'развлечение и отдых',
  LANDUSE = 'зарезервированная территория',
  NATURAL = 'природа',
  OFFICE = 'офис',
  WATERWAY = 'прибрежная зона',
  CRAFT = 'ручная работа',
  MAN_MADE = 'рукотворный объект',
  HIGHWAY = 'что-то на шоссе',
  BUILDING = 'здание',
  INDUSTRIAL = 'промышленный район',
  RESIDENTAL = 'жилой район',
}

export const AddressComponentTypeRU = {
  ...AddressComponentTypeAreaRU,
  ...AddressComponentTypeLocalityRU,
  ...AddressComponentTypeLocalityWayRU,
  ...AddressComponentTypeHouseRU,
  ...AddressComponentTypeHouseDetailRU,
  ...AddressComponentTypePlaceRU,
};
export type AddressComponentTypeRU = typeof AddressComponentTypeRU;
