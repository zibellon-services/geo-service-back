import { ResAPIV1Component } from './classes';
import { AddressComponentZone } from './constants';

type MapExtrasResult = {
  displayName: string; //короткое название адреса
  displayDescription: string; //Полное название адреса (С типами компонентов)
  shortDescription: string; //Короткое название адреса (БЕЗ типов компонентов)
};

export function mapComponentListToDisplayInfo(componentList: ResAPIV1Component[]): MapExtrasResult {
  let displayName = '';
  let displayDescription = '';
  let shortDescription = '';
  let displayDescriptionList: string[] = [];
  let shortDescriptionList: string[] = [];

  //Бежим по списку с компонентами
  for (const el of componentList) {
    //Если это НЕ PLACE -> добавляем в описание
    if (el.zone !== AddressComponentZone.PLACE) {
      displayDescriptionList.push(el.type !== '' ? el.type + ' ' + el.name : el.name);
    } else {
      //Если это PLACE -> ставим в displayName
      displayName = el.name;
    }
    shortDescriptionList.push(el.name);
  }

  displayDescription = displayDescriptionList.join(', ');
  shortDescription = shortDescriptionList.join(', ');

  if (displayName === '') {
    displayName = displayDescription;
  }

  return {
    displayName,
    displayDescription,
    shortDescription,
  };
}

export function mapComponentListExtras(componentList: ResAPIV1Component[]) {
  if (componentList.length === 0) {
    return {
      type: '', //На русском
      typeKey: '', //На английском
      zone: '', //Название зоны - НА АНГЛ
    };
  }
  const lastComponent = componentList[componentList.length - 1];
  return {
    type: lastComponent.type,
    typeKey: lastComponent.typeKey,
    zone: lastComponent.zone,
  };
}
