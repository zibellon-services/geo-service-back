import { NetClient } from 'core/net-client/net-client';
import { getProcessEnv } from './utils-env-config';

export const OSM_API = new NetClient().setHost(getProcessEnv().OSM_HOST_ADDRESS);
export const OSRM_API = new NetClient().setHost(getProcessEnv().OSRM_HOST_ADDRESS);
export const PHOTON_API = new NetClient().setHost(getProcessEnv().PHOTON_HOST_ADDRESS);
