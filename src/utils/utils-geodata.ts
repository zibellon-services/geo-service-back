import { AddressComponent } from 'database/postgres/models/final/address-component.model';
import { GeoData } from 'database/postgres/models/final/geo-data.model';
import { GeoDataCreateReqDto } from 'modules/dto/common/geo.dto';
import { Sequelize, WhereOptions } from 'sequelize';
import { AddressComponentZone } from './constants';

interface IGeoDataArrays {
  point: number[];
  polygon: number[][];
}

export function mapGeoDataToArrays(geoData: GeoDataCreateReqDto): IGeoDataArrays {
  const point: number[] = [geoData.point.lat, geoData.point.lng];
  let polygon: number[][] = geoData.polygon.map((el) => {
    return [el.lat, el.lng];
  });

  const firstLatLng = geoData.polygon[0];
  const lastLatLng = geoData.polygon[geoData.polygon.length - 1];

  if (firstLatLng.lat !== lastLatLng.lat && firstLatLng.lng !== lastLatLng.lng) {
    polygon.push([firstLatLng.lat, firstLatLng.lng]);
  }

  return {
    point,
    polygon,
  };
}

export function mapGeoDataToCreateObj(geoData: GeoDataCreateReqDto): {} {
  const geoDataArrays = mapGeoDataToArrays(geoData);
  return {
    point: {
      type: 'Point',
      coordinates: geoDataArrays.point,
    },
    polygon: {
      type: 'Polygon',
      coordinates: [geoDataArrays.polygon],
    },
  };
}

function getEqualGeoDataWhere(geoDataArrays: IGeoDataArrays): WhereOptions {
  return {
    point: Sequelize.fn(
      'ST_GeomFromGeoJSON',
      `{"type":"Point","coordinates":[${geoDataArrays.point[0]}, ${geoDataArrays.point[1]}]}`
    ),
    polygon: Sequelize.fn(
      'ST_GeomFromGeoJSON',
      `{"type":"Polygon","coordinates":[[${geoDataArrays.polygon.map((el) => `[${el[0]}, ${el[1]}]`).join(',')}]]}`
    ),
  };
}

export async function findEqualGeoDataSimple(geoData: GeoDataCreateReqDto): Promise<GeoData | null> {
  const geoDataArrays = mapGeoDataToArrays(geoData);
  return await GeoData.findOne({
    where: getEqualGeoDataWhere(geoDataArrays),
  });
}

export async function findEqualGeoDataPlace(geoData: GeoDataCreateReqDto): Promise<GeoData | null> {
  const geoDataArrays = mapGeoDataToArrays(geoData);

  return await GeoData.findOne({
    where: getEqualGeoDataWhere(geoDataArrays),
    include: [
      {
        model: AddressComponent,
        duplicating: false,
        where: {
          zone: AddressComponentZone.PLACE,
        },
      },
    ],
  });
}
