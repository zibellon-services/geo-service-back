import { ResAPIV1Address, ResAPIV1Component } from './classes';
import { AddressComponentZone } from './constants';
import { AddressComponentTypeLocalityEN, AddressComponentTypePlaceEN } from './constants-en';
import { AddressComponentTypeLocalityRU, AddressComponentTypePlaceRU } from './constants-ru';
import { mapComponentListExtras, mapComponentListToDisplayInfo } from './utils-mapper';
import {
  parseRegexArea,
  parseRegexHouseAndHouseDetails,
  parseRegexLocality,
  parseRegexLocalityDetails,
  parseRegexLocalityWay,
} from './utils-parse-regex';

export function mapOSMAddressToAPIV1Response(osmResponse: any): ResAPIV1Address {
  const componentList: ResAPIV1Component[] = mapOsmDetailsToAPIV1ComponentList(osmResponse.address);
  return {
    osmId: Number(osmResponse.osm_id),
    osmType: String(osmResponse.osm_type),
    osmPlaceClass: String(osmResponse.class),
    osmPlaceType: String(osmResponse.type),
    ...mapComponentListToDisplayInfo(componentList),
    ...mapComponentListExtras(componentList),
    coordinates: {
      lat: parseFloat(osmResponse.lat),
      lng: parseFloat(osmResponse.lon),
    },
    componentList,
  };
}

export function mapOsmDetailsToAPIV1ComponentList(osmAddress: any): ResAPIV1Component[] {
  const resultComponentList: ResAPIV1Component[] = [];

  //MUNICIPALITY+CITY = W414623183
  //allotments+village = W226276138

  //Найти: hamlet+allotments,
  //suburb - NO_REG!

  // "class": "leisure",
  // "type": "ice_rink",

  //AREA = state+REG
  //AREA_DETAILS = county+REG

  //LOCALITY = allotments+REG, hamlet, village, town, city
  //if (town && allotments) -> LOCALITY = allotments
  //if (town && hamlet) -> LOCALITY = hamlet
  //if (hamlet && village) -> LOCALITY = hamlet

  //LOCALITY_DETAILS = municipality+REG, city_district+REG, suburb
  //if (municipality && city_district) -> LOCALITY_DETAILS = municipality+REG
  //if (city_district && suburb) -> LOCALITY_DETAILS = city_district+REG
  //if (!LOCALITY && municipality) -> LOCALITY = municipality+REG

  //LOCALITY_WAY = road+REG,
  //HOUSE =

  //----------------------
  // Zone - AREA
  //----------------------
  const componentArea: ResAPIV1Component = {
    zone: AddressComponentZone.AREA,
    type: '',
    typeKey: '',
    name: '',
  };

  if (typeof osmAddress.state === 'string') {
    const parsedOsmAddress = parseRegexArea(osmAddress.state);
    componentArea.name = parsedOsmAddress.name;
    componentArea.type = parsedOsmAddress.type;
    componentArea.typeKey = parsedOsmAddress.typeKey;
  }
  if (componentArea.name.length > 0) {
    resultComponentList.push(componentArea);
  }

  //----------------------
  // Zone - AREA_DETAILS
  //----------------------
  const componentAreaDetails: ResAPIV1Component = {
    zone: AddressComponentZone.AREA_DETAILS,
    type: '',
    typeKey: '',
    name: '',
  };

  if (typeof osmAddress.county === 'string') {
    //Типо ... Городской округ
    const parsedOsmAddress = parseRegexArea(osmAddress.county);
    componentAreaDetails.name = parsedOsmAddress.name;
    componentAreaDetails.type = parsedOsmAddress.type;
    componentAreaDetails.typeKey = parsedOsmAddress.typeKey;
  }
  if (componentAreaDetails.name.length > 0) {
    resultComponentList.push(componentAreaDetails);
  }

  //----------------------
  // Zone - LOCALITY
  //----------------------
  const componentLocality: ResAPIV1Component = {
    zone: AddressComponentZone.LOCALITY,
    type: '',
    typeKey: '',
    name: '',
  };

  if (typeof osmAddress.allotments === 'string') {
    // СНТ, СОТ. СОТ "Малинка" Росийское СНТ == Буржуйский огород
    const parsedOsmAddress = parseRegexLocality(osmAddress.allotments);
    componentLocality.name = parsedOsmAddress.name;
    componentLocality.type = parsedOsmAddress.type;
    componentLocality.typeKey = parsedOsmAddress.typeKey;
  } else if (typeof osmAddress.hamlet === 'string') {
    //поселок
    componentLocality.type = AddressComponentTypeLocalityRU.HAMLET;
    componentLocality.typeKey = AddressComponentTypeLocalityEN.HAMLET;
    componentLocality.name = osmAddress.hamlet; //Гамлет это поселок
  } else if (typeof osmAddress.town === 'string') {
    //поселок
    componentLocality.type = AddressComponentTypeLocalityRU.HAMLET;
    componentLocality.typeKey = AddressComponentTypeLocalityEN.HAMLET;
    componentLocality.name = osmAddress.town;
  } else if (typeof osmAddress.village === 'string') {
    //Деревня
    componentLocality.type = AddressComponentTypeLocalityRU.VILLAGE;
    componentLocality.typeKey = AddressComponentTypeLocalityEN.VILLAGE;
    componentLocality.name = osmAddress.village;
  } else if (typeof osmAddress.city === 'string') {
    //Большой город
    componentLocality.type = AddressComponentTypeLocalityRU.CITY;
    componentLocality.typeKey = AddressComponentTypeLocalityEN.CITY;
    componentLocality.name = osmAddress.city;
  }

  //ЗАПОМНИТЬ !!!
  //Если нет НАСЕЛЕННОГО пункта
  const replaceLocalityWithDetails = componentLocality.name.length === 0 && typeof osmAddress.municipality === 'string';
  if (replaceLocalityWithDetails) {
    const parsedOsmAddress = parseRegexLocalityDetails(osmAddress.municipality);
    componentLocality.name = parsedOsmAddress.name;
    componentLocality.type = parsedOsmAddress.type;
    componentLocality.typeKey = parsedOsmAddress.typeKey;
  }

  if (componentLocality.name.length > 0) {
    resultComponentList.push(componentLocality);
  }

  //----------------------
  // Zone - LOCALITY_DETAILS
  //----------------------
  const componentLocalityDetails: ResAPIV1Component = {
    zone: AddressComponentZone.LOCALITY_DETAILS,
    type: '',
    typeKey: '',
    name: '',
  };

  // municipality+REG, city_district+REG, suburb
  if (typeof osmAddress.municipality === 'string') {
    const parsedOsmAddress = parseRegexLocalityDetails(osmAddress.municipality);
    componentLocalityDetails.name = parsedOsmAddress.name;
    componentLocalityDetails.type = parsedOsmAddress.type;
    componentLocalityDetails.typeKey = parsedOsmAddress.typeKey;
  } else if (typeof osmAddress.city_district === 'string') {
    const parsedOsmAddress = parseRegexLocalityDetails(osmAddress.city_district);
    componentLocalityDetails.name = parsedOsmAddress.name;
    componentLocalityDetails.type = parsedOsmAddress.type;
    componentLocalityDetails.typeKey = parsedOsmAddress.typeKey;
  } else if (typeof osmAddress.suburb === 'string') {
    const parsedOsmAddress = parseRegexLocalityDetails(osmAddress.suburb);
    componentLocalityDetails.name = parsedOsmAddress.name;
    componentLocalityDetails.type = parsedOsmAddress.type;
    componentLocalityDetails.typeKey = parsedOsmAddress.typeKey;
  }

  //ЗАПОМНИТЬ !!!
  if (componentLocalityDetails.name.length > 0) {
    if (!replaceLocalityWithDetails) {
      resultComponentList.push(componentLocalityDetails);
    }
  }

  // Zone - LOCALITY_WAY

  // НЕТ РАЗДЕЛЕНИЯ на улица/шоссе/площадь/...
  if (typeof osmAddress.road === 'string') {
    const parsedOsmAddress = parseRegexLocalityWay(osmAddress.road);
    resultComponentList.push(parsedOsmAddress);
  } else if (typeof osmAddress.pedestrian === 'string') {
    //НЕТ HIGWAY
    const parsedOsmAddress = parseRegexLocalityWay(osmAddress.pedestrian);
    resultComponentList.push(parsedOsmAddress);
  }

  // Zone 2, house + Zone 1, house details
  if (typeof osmAddress.house_number === 'string') {
    resultComponentList.push(...parseRegexHouseAndHouseDetails(osmAddress.house_number));
  }

  //Zone - Place
  const componentPlace: ResAPIV1Component = {
    zone: AddressComponentZone.PLACE,
    type: '',
    typeKey: '',
    name: '',
  };

  if (typeof osmAddress.place === 'string') {
    componentPlace.type = AddressComponentTypePlaceRU.SIMPLE_PLACE;
    componentPlace.typeKey = AddressComponentTypePlaceEN.SIMPLE_PLACE;
    componentPlace.name = osmAddress.place;
  } else if (typeof osmAddress.amenity === 'string') {
    componentPlace.type = AddressComponentTypePlaceRU.AMENITY;
    componentPlace.typeKey = AddressComponentTypePlaceEN.AMENITY;
    componentPlace.name = osmAddress.amenity;
  } else if (typeof osmAddress.tourism === 'string') {
    componentPlace.type = AddressComponentTypePlaceRU.TOURISM;
    componentPlace.typeKey = AddressComponentTypePlaceEN.TOURISM;
    componentPlace.name = osmAddress.tourism;
  } else if (typeof osmAddress.leisure === 'string') {
    componentPlace.type = AddressComponentTypePlaceRU.JOYANDREST; // Дословный перевод
    componentPlace.typeKey = AddressComponentTypePlaceEN.JOYANDREST;
    componentPlace.name = osmAddress.leisure;
  } else if (typeof osmAddress.shop === 'string') {
    componentPlace.type = AddressComponentTypePlaceRU.SHOP;
    componentPlace.typeKey = AddressComponentTypePlaceEN.SHOP;
    componentPlace.name = osmAddress.shop;
  } else if (typeof osmAddress.natural === 'string') {
    componentPlace.type = AddressComponentTypePlaceRU.NATURAL;
    componentPlace.typeKey = AddressComponentTypePlaceEN.NATURAL;
    componentPlace.name = osmAddress.natural;
  } else if (typeof osmAddress.historic === 'string') {
    componentPlace.type = AddressComponentTypePlaceRU.HISTORIC;
    componentPlace.typeKey = AddressComponentTypePlaceEN.HISTORIC;
    componentPlace.name = osmAddress.historic;
  } else if (typeof osmAddress.landuse === 'string') {
    componentPlace.type = AddressComponentTypePlaceRU.LANDUSE;
    componentPlace.typeKey = AddressComponentTypePlaceEN.LANDUSE;
    componentPlace.name = osmAddress.landuse;
  } else if (typeof osmAddress.railway === 'string') {
    componentPlace.type = AddressComponentTypePlaceRU.RAILWAY;
    componentPlace.typeKey = AddressComponentTypePlaceEN.RAILWAY;
    componentPlace.name = osmAddress.railway;
  } else if (typeof osmAddress.aeroway === 'string') {
    componentPlace.type = AddressComponentTypePlaceRU.AIRPORT;
    componentPlace.typeKey = AddressComponentTypePlaceEN.AIRPORT;
    componentPlace.name = osmAddress.aeroway;
  } else if (typeof osmAddress.office === 'string') {
    componentPlace.type = AddressComponentTypePlaceRU.OFFICE;
    componentPlace.typeKey = AddressComponentTypePlaceEN.OFFICE;
    componentPlace.name = osmAddress.office;
  } else if (typeof osmAddress.waterway === 'string') {
    componentPlace.type = AddressComponentTypePlaceRU.WATERWAY;
    componentPlace.typeKey = AddressComponentTypePlaceEN.WATERWAY;
    componentPlace.name = osmAddress.waterway;
  } else if (typeof osmAddress.craft === 'string') {
    componentPlace.type = AddressComponentTypePlaceRU.CRAFT;
    componentPlace.typeKey = AddressComponentTypePlaceEN.CRAFT;
    componentPlace.name = osmAddress.craft;
  } else if (typeof osmAddress.man_made === 'string') {
    componentPlace.type = AddressComponentTypePlaceRU.MAN_MADE;
    componentPlace.typeKey = AddressComponentTypePlaceEN.MAN_MADE;
    componentPlace.name = osmAddress.man_made;
  } else if (typeof osmAddress.highway === 'string') {
    componentPlace.type = AddressComponentTypePlaceRU.HIGHWAY;
    componentPlace.typeKey = AddressComponentTypePlaceEN.HIGHWAY;
    componentPlace.name = osmAddress.highway;
  } else if (typeof osmAddress.building === 'string') {
    componentPlace.type = AddressComponentTypePlaceRU.BUILDING;
    componentPlace.typeKey = AddressComponentTypePlaceEN.BUILDING;
    componentPlace.name = osmAddress.building;
  } else if (typeof osmAddress.industrial === 'string') {
    componentPlace.type = AddressComponentTypePlaceRU.INDUSTRIAL;
    componentPlace.typeKey = AddressComponentTypePlaceEN.INDUSTRIAL;
    componentPlace.name = osmAddress.industrial;
  } else if (typeof osmAddress.residential === 'string') {
    componentPlace.type = AddressComponentTypePlaceRU.RESIDENTAL;
    componentPlace.typeKey = AddressComponentTypePlaceEN.RESIDENTAL;
    componentPlace.name = osmAddress.residential;
  }

  if (componentPlace.name.length > 0) {
    resultComponentList.push(componentPlace);
  }

  return resultComponentList;
}
