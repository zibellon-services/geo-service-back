//Преобразование ПРАВОГО значения (РУССКОЕ/ENGLISH) -> левый KEY
export const enumValToKey = (val: any, o: Record<string, string>) => {
  const entries = Object.entries(o);
  const result = entries.find((el) => el[1] === val);
  if (result) {
    return result[0];
  }
  return entries[0][0];
};

//Преобразование ЛЕВОГО значения (KEY) (ENGLISH) -> ПРАВЫЙ VALUE
export const enumKeyToVal = (key: string, o: Record<string, string>) => {
  const entries = Object.entries(o);
  const result = entries.find((el) => el[0] === key);
  if (result) {
    return result[1];
  }
  return entries[0][1];
};
