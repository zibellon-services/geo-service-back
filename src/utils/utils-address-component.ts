import { AddressComponent } from 'database/postgres/models/final/address-component.model';
import { Address } from 'database/postgres/models/final/address.model';
import { GeoData } from 'database/postgres/models/final/geo-data.model';
import { Transaction } from 'sequelize';
import { AddressComponentZone, AddressType } from './constants';
import { AddressComponentTypeRU } from './constants-ru';
import { dbRunTrx } from './utils-db';
import { enumKeyToVal } from './utils-map-enums';

export function formatComponentZoneToKeyId(zone: string) {
  switch (zone) {
    case AddressComponentZone.AREA:
    case AddressComponentZone.LOCALITY:
    case AddressComponentZone.HOUSE:
    case AddressComponentZone.PLACE:
      return zone.toLocaleLowerCase().concat('Id');

    case AddressComponentZone.LOCALITY_WAY:
      return zone.toLocaleLowerCase().replace('_w', 'W').concat('Id');

    case AddressComponentZone.AREA_DETAILS:
    case AddressComponentZone.LOCALITY_DETAILS:
      return zone.toLocaleLowerCase().replace('_d', 'D').concat('Id');

    // case AddressComponentZone.HOUSE_DETAILS_1:
    // case AddressComponentZone.HOUSE_DETAILS_1:
    default:
      return zone.toLocaleLowerCase().replace('_d', 'D').replace('_1', '1').replace('_2', '2').concat('Id');
  }
}

export function formatComponentZoneToSearchKey(zone: string) {
  let lowerCaseZone = zone.toLocaleLowerCase();
  let firstUpperCaseZone = lowerCaseZone.charAt(0).toUpperCase() + lowerCaseZone.slice(1);

  switch (zone) {
    case AddressComponentZone.AREA:
    case AddressComponentZone.LOCALITY:
    case AddressComponentZone.HOUSE:
    case AddressComponentZone.PLACE:
      return 'search' + firstUpperCaseZone;

    case AddressComponentZone.LOCALITY_WAY:
      return 'search' + firstUpperCaseZone.replace('_w', 'W');

    case AddressComponentZone.AREA_DETAILS:
    case AddressComponentZone.LOCALITY_DETAILS:
      return 'search' + firstUpperCaseZone.replace('_d', 'D');

    // case AddressComponentZone.HOUSE_DETAILS_1:
    // case AddressComponentZone.HOUSE_DETAILS_1:
    default:
      return 'search' + firstUpperCaseZone.replace('_d', 'D').replace('_1', '1').replace('_2', '2');
  }
}

export async function addressComponentAfterUpdateTransaction(
  addressComponent: AddressComponent,
  transaction: Transaction
) {
  const searchValue =
    addressComponent.zone === AddressComponentZone.PLACE
      ? addressComponent.name
      : enumKeyToVal(String(addressComponent.type), AddressComponentTypeRU) + ' ' + addressComponent.name;

  await dbRunTrx(
    Address.update(
      {
        [formatComponentZoneToSearchKey(addressComponent.zone)]: searchValue,
      },
      {
        where: {
          [formatComponentZoneToKeyId(addressComponent.zone)]: addressComponent.id,
        },
        transaction,
      }
    ),
    transaction
  );
}

export async function createAddressForComponentTransaction(component: AddressComponent, transaction: Transaction) {
  //Добавляем Id + название поля
  let addressCreateObj = {
    [formatComponentZoneToKeyId(component.zone)]: component.id,
    geoDataId: component.geoData?.id,
    targetComponentId: component.id,
  };

  const componentSearchKey = formatComponentZoneToSearchKey(component.zone);

  if (component.zone === AddressComponentZone.PLACE) {
    addressCreateObj['type'] = AddressType.PLACE;
    addressCreateObj[componentSearchKey] = component.name;
  } else {
    addressCreateObj[componentSearchKey] =
      enumKeyToVal(String(component.type), AddressComponentTypeRU) + ' ' + component.name;
  }

  let parentId = component.parentId;

  while (parentId) {
    const parentComponent = (await AddressComponent.findByPk(parentId))!;
    parentId = parentComponent.parentId;

    addressCreateObj[formatComponentZoneToKeyId(parentComponent.zone)] = parentComponent.id;
    addressCreateObj[formatComponentZoneToSearchKey(parentComponent.zone)] =
      enumKeyToVal(String(parentComponent.type), AddressComponentTypeRU) + ' ' + parentComponent.name;
  }

  return await dbRunTrx(
    Address.create(
      {
        ...addressCreateObj,
      },
      {
        transaction,
      }
    ),
    transaction
  );
}

export async function updateAddressGeoDataByComponentTransaction(geoData: GeoData, transaction: Transaction) {
  await dbRunTrx(
    Address.update(
      {
        geoDataId: geoData.id,
      },
      {
        where: {
          targetComponentId: geoData.componentId,
        },
        transaction,
      }
    ),
    transaction
  );
}
