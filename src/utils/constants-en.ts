export enum AddressComponentTypeAreaEN {
  COUNTY = 'COUNTY',
  CITY_COUNTY = 'CITY_COUNTY',
  AREA = 'AREA',
  REGION = 'REGION',
  EDGE = 'EDGE',
  REPUBLIC = 'REPUBLIC',
}

export enum AddressComponentTypeLocalityEN {
  CITY = 'CITY',
  VILLAGE = 'VILLAGE',
  SUBURB = 'SUBURB',
  HAMLET = 'HAMLET',
  SELO = 'SELO',
  COUNTY = 'COUNTY',
  RURAL_SETTELMENT = 'RURAL_SETTELMENT',
  COTTAGE_SETTELMENT = 'COTTAGE_SETTELMENT',
  ST = 'ST',
  SNT = 'SNT',
  SOT = 'SOT',
  SONT = 'SONT',
  KIZ = 'KIZ',
  TSO = 'TSO',
  TIZ = 'TIZ',
}

export enum AddressComponentTypeLocalityWayEN {
  STREET = 'STREET',
  LINE = 'LINE',
  ROADWAY = 'ROADWAY',
  SQUARE = 'SQUARE',
  PARK = 'PARK',
  AVENUE = 'AVENUE',
  TRAVEL = 'TRAVEL',
  LANE = 'LANE',
  BOULEVARD = 'BOULEVARD',
  EMBANKMENT = 'EMBANKMENT',
}

export enum AddressComponentTypeHouseEN {
  HOUSE = 'HOUSE',
}

export enum AddressComponentTypeHouseDetailEN {
  STRUCTURE = 'STRUCTURE',
  CORPUS = 'CORPUS',
  ANNEX = 'ANNEX',
}

export enum AddressComponentTypePlaceEN {
  SIMPLE_PLACE = 'SIMPLE_PLACE',
  AIRPORT = 'AIRPORT',
  SHOP = 'SHOP',
  SPORT = 'SPORT',
  TOURISM = 'TOURISM',
  HISTORIC = 'HISTORIC',
  RAILWAY = 'RAILWAY',
  AMENITY = 'AMENITY',
  JOYANDREST = 'JOYANDREST',
  LANDUSE = 'LANDUSE',
  NATURAL = 'NATURAL',
  OFFICE = 'OFFICE',
  WATERWAY = 'WATERWAY',
  CRAFT = 'CRAFT',
  MAN_MADE = 'MAN_MADE',
  HIGHWAY = 'HIGHWAY',
  BUILDING = 'BUILDING',
  INDUSTRIAL = 'INDUSTRIAL',
  RESIDENTAL = 'RESIDENTAL',
}

export const AddressComponentTypeEN = {
  ...AddressComponentTypeAreaEN,
  ...AddressComponentTypeLocalityEN,
  ...AddressComponentTypeLocalityWayEN,
  ...AddressComponentTypeHouseEN,
  ...AddressComponentTypeHouseDetailEN,
  ...AddressComponentTypePlaceEN,
};
export type AddressComponentTypeEN = typeof AddressComponentTypeEN;
