import { AddressComponentZone } from './constants';

//Получение индекса зоны по ее нащванию (EN)
export function getZoneIndex(zone: string) {
  switch (zone) {
    case AddressComponentZone.AREA:
      return 10;
    case AddressComponentZone.AREA_DETAILS:
      return 20;
    case AddressComponentZone.LOCALITY:
      return 30;
    case AddressComponentZone.LOCALITY_DETAILS:
      return 40;
    case AddressComponentZone.LOCALITY_WAY:
      return 50;
    case AddressComponentZone.HOUSE:
      return 60;
    case AddressComponentZone.HOUSE_DETAILS_1:
      return 70;
    case AddressComponentZone.HOUSE_DETAILS_2:
      return 80;
    case AddressComponentZone.PLACE:
      return 90;
    default:
      return 100;
  }
}

//Какие то там форматтеры
export function mapSpacesSearchValue(searchValue: string): string {
  return searchValue.replace(/\,/g, ' ').replace(/\s\s+/g, ' ');
}

export function escapeSQL(dangerousString: string) {
  return dangerousString
    .trim()
    .replace(/\</g, '')
    .replace(/\>/g, '')
    .replace(/\@/g, '')
    .replace(/\%/g, '')
    .replace(/\$/g, '')
    .replace(/\!/g, '')
    .replace(/\:/g, '')
    .replace(/\|/g, '')
    .replace(/\'/g, '')
    .replace(/\"/g, '')
    .replace(/\(/g, '')
    .replace(/\)/g, '');
}

export function getOsmIdsFromPhotonArray(features: any[]) {
  const osmIdSet = new Set<string>();
  for (const el of features) {
    const osmId = ((el.properties.osm_type as string) + el.properties?.osm_id) as string;
    osmIdSet.add(osmId);
  }
  return [...osmIdSet].join(',');
}
