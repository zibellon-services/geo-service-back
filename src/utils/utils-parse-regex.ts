import { RegexToComponentType, ResAPIV1Component } from './classes';
import { AddressComponentZone, CONSTANTS } from './constants';
import { AddressComponentTypeHouseDetailEN, AddressComponentTypeHouseEN } from './constants-en';
import { AddressComponentTypeHouseDetailRU, AddressComponentTypeHouseRU, AddressComponentTypeRU } from './constants-ru';
import { enumValToKey } from './utils-map-enums';

function parseRgex(unknown: string, zone: AddressComponentZone): ResAPIV1Component {
  const component: ResAPIV1Component = {
    zone,
    type: '',
    typeKey: '',
    name: unknown,
  };

  let regexArr: RegExpExecArray | null = null;

  if (zone === AddressComponentZone.AREA || zone === AddressComponentZone.AREA_DETAILS) {
    regexArr = CONSTANTS.AREA_REGEX.exec(unknown);
  } else if (zone === AddressComponentZone.LOCALITY || zone === AddressComponentZone.LOCALITY_DETAILS) {
    regexArr = CONSTANTS.LOCALITY_REGEX.exec(unknown);
  } else if (zone === AddressComponentZone.LOCALITY_WAY) {
    regexArr = CONSTANTS.LOCALITY_WAY_REGEX.exec(unknown);
  }

  if (regexArr !== null) {
    return {
      zone,
      ...parseRegexToComponent(unknown, regexArr),
    };
  }

  return component;
}

function parseRegexToComponent(unknown: string, array: RegExpExecArray): RegexToComponentType {
  return {
    type: array[2], //РУССКОЕ название
    typeKey: enumValToKey(array[2], AddressComponentTypeRU), //EnglishKey
    name: unknown.replace(array[2], '').trim().replace('  ', ' ').replace(' , ', ', '), //Название ЧИСТОЕ
  };
}

//Сам процесс парсинга
export function parseRegexArea(unknown: string): ResAPIV1Component {
  return parseRgex(unknown, AddressComponentZone.AREA);
}

export function parseRegexAreaDetails(unknown: string): ResAPIV1Component {
  return parseRgex(unknown, AddressComponentZone.AREA_DETAILS);
}

export function parseRegexLocality(unknown: string): ResAPIV1Component {
  return parseRgex(unknown, AddressComponentZone.LOCALITY);
}

export function parseRegexLocalityDetails(unknown: string): ResAPIV1Component {
  return parseRgex(unknown, AddressComponentZone.LOCALITY_DETAILS);
}

//УЛИЦА и тд
export function parseRegexLocalityWay(unknown: string): ResAPIV1Component {
  return parseRgex(unknown, AddressComponentZone.LOCALITY_WAY);
}

//Парсинг ДОМА + КОРПУС + СТРОЕНИЕ
export function parseRegexHouseAndHouseDetails(unknownHouse: string): ResAPIV1Component[] {
  let localHouse = unknownHouse.replace(/ /, '');

  const houseDetailsKorpRegx = /к([0-9-/]+)/i;
  const houseDetailsStroenieRegx = /с([0-9-/]+)/i;

  let resultList: ResAPIV1Component[] = [];

  //Ишем КООПУС
  let korpus = houseDetailsKorpRegx.exec(localHouse);

  //Ищем строение
  let stroenie = houseDetailsStroenieRegx.exec(localHouse);

  // Log.info('Korpus =', korpus);
  // Log.info('Stroenie =', stroenie);

  //Если у нашего дома НЕТ НИ корпуса НИ строения
  if (korpus === null && stroenie === null) {
    return [
      {
        zone: AddressComponentZone.HOUSE,
        type: AddressComponentTypeHouseRU.HOUSE,
        typeKey: AddressComponentTypeHouseEN.HOUSE,
        name: localHouse,
      },
    ];
  }

  //Если оба есть - и корпус и строение
  if (korpus && stroenie) {
    let korpusComponent = {
      zone: AddressComponentZone.HOUSE_DETAILS_1,
      type: AddressComponentTypeHouseDetailRU.CORPUS,
      typeKey: AddressComponentTypeHouseDetailEN.CORPUS,
      name: korpus[1],
    };

    let stroenieComponent = {
      zone: AddressComponentZone.HOUSE_DETAILS_2,
      type: AddressComponentTypeHouseDetailRU.STRUCTURE,
      typeKey: AddressComponentTypeHouseDetailEN.STRUCTURE,
      name: stroenie[1],
    };

    //Первый идет корпус
    if (korpus.index < stroenie.index) {
      resultList.push(korpusComponent);
      resultList.push(stroenieComponent);
    } else {
      //Первое идет строение
      resultList.push(stroenieComponent);
      resultList.push(korpusComponent);
    }

    //Вырезаем их из ИСХОДНОЙ строки
    localHouse = localHouse.replace(korpus[0], '').replace(stroenie[0], '');
  } else if (korpus) {
    //Есть только ОДИН из двух - (или СТРОЕНИЕ или КОРПУС)
    //есть корпус
    resultList.push({
      zone: AddressComponentZone.HOUSE_DETAILS_1,
      type: AddressComponentTypeHouseDetailRU.CORPUS,
      typeKey: AddressComponentTypeHouseDetailEN.CORPUS,
      name: korpus[1],
    });

    //Вырезаем их из ИСХОДНОЙ строки
    localHouse = localHouse.replace(korpus[0], '');
  } else if (stroenie) {
    //есть строение
    resultList.push({
      zone: AddressComponentZone.HOUSE_DETAILS_1,
      type: AddressComponentTypeHouseDetailRU.STRUCTURE,
      typeKey: AddressComponentTypeHouseDetailEN.STRUCTURE,
      name: stroenie[1],
    });

    //Вырезаем их из ИСХОДНОЙ строки
    localHouse = localHouse.replace(stroenie[0], '');
  }

  //Добавляем САМ дом - в первую позицию
  resultList.unshift({
    zone: AddressComponentZone.HOUSE,
    type: AddressComponentTypeHouseRU.HOUSE,
    typeKey: AddressComponentTypeHouseEN.HOUSE,
    name: localHouse,
  });

  return resultList;
}
