import { AddressComponentZone } from './constants';

export type ResAPIV1Address = {
  osmId?: number;
  osmType?: string;
  osmPlaceType?: string;
  osmPlaceClass?: string;
  dbId?: string;
  displayName: string; //короткое название адреса
  displayDescription: string; //Полное название адреса (С типами компонентов)
  shortDescription: string; //Короткое название адреса (БЕЗ типов компонентов)
  type: string;
  typeKey: string;
  zone: string;
  coordinates: ResAPIV1Coordinates;
  componentList: ResAPIV1Component[];
};

export type ResAPIV1Component = {
  zone: AddressComponentZone;
  type: string;
  typeKey: string;
  name: string;
};

export type ResAPIV1Coordinates = {
  lat: number;
  lng: number;
};

export type RegexToComponentType = {
  type: string;
  typeKey: string;
  name: string;
};

export type PriorityQueryParams = {
  areaPriority?: string;
  localityPriority?: string;
  localityWayPriority?: string;
};
