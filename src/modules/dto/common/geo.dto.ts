import { extendApi } from '@anatine/zod-openapi';
import { z } from 'zod';

export const latLngCreateReq = extendApi(
  z.object({
    lat: extendApi(z.number(), {
      example: 64.5344277,
    }),
    lng: extendApi(z.number(), {
      example: 64.5344277,
    }),
  })
);
export type LatLngCreateReqDto = z.infer<typeof latLngCreateReq>;

export const geoDataCreateReq = extendApi(
  z.object({
    point: latLngCreateReq,
    polygon: latLngCreateReq.array(),
  })
);
export type GeoDataCreateReqDto = z.infer<typeof geoDataCreateReq>;
