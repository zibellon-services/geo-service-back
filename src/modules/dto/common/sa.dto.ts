import { extendApi } from '@anatine/zod-openapi';
import { z } from 'zod';
import { addressComponentDefaultRes } from '../default/address-component-default.dto';
import { addressDefaultRes } from '../default/address-default.dto';
import { geoDataDefaultRes } from '../default/geo-data-default.dto';

//BOTH
export const saComponentIdReqRes = extendApi(
  z.object({
    componentId: extendApi(z.string().uuid(), {
      example: '69e45d20-98a9-4c75-ab60-1188d64d510a',
    }),
  })
);
export type SAComponentIdReqResDto = z.infer<typeof saComponentIdReqRes>;

//REQ
//...

//RES
export const saComponentGetOneRes = z.object({
  addressComponentList: addressComponentDefaultRes
    .merge(
      z.object({
        geoData: geoDataDefaultRes,
        address: addressDefaultRes,
      })
    )
    .array(),
  address: addressDefaultRes,
});

export const saComponentGetListRes = z
  .object({
    addressComponentList: addressComponentDefaultRes
      .merge(
        z.object({
          geoData: geoDataDefaultRes,
          address: addressDefaultRes,
        })
      )
      .array(),
    address: addressDefaultRes,
  })
  .array();
