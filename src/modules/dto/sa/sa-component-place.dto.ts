import { extendApi } from '@anatine/zod-openapi';
import { AddressComponentTypePlaceEN } from 'utils/constants-en';
import { z } from 'zod';
import { geoDataCreateReq } from '../common/geo.dto';

export const saComponentPlaceGetListReq = extendApi(
  z.object({
    componentId: extendApi(z.string().uuid().optional(), {
      example: '69e45d20-98a9-4c75-ab60-1188d64d510a',
    }),
    searchValue: extendApi(z.string().optional(), {
      example: 'SomeName',
    }),
  })
);
export type SAComponentPlaceGetListReqDto = z.infer<typeof saComponentPlaceGetListReq>;

export const saComponentPlaceCreateReq = extendApi(
  z.object({
    parentComponentId: extendApi(z.string().uuid(), {
      example: '69e45d20-98a9-4c75-ab60-1188d64d510a',
    }),
    name: extendApi(z.string().min(1), {
      example: 'SomeName',
    }),
    type: extendApi(z.nativeEnum(AddressComponentTypePlaceEN), {
      example: Object.values(AddressComponentTypePlaceEN).join('/'),
    }),
    geoData: geoDataCreateReq,
  })
);
export type SAComponentPlaceCreateReqDto = z.infer<typeof saComponentPlaceCreateReq>;

export const saComponentPlaceUpdateReq = extendApi(
  z.object({
    componentId: extendApi(z.string().uuid(), {
      example: '69e45d20-98a9-4c75-ab60-1188d64d510a',
    }),
    name: extendApi(z.string().min(1), {
      example: 'SomeName',
    }),
    type: extendApi(z.nativeEnum(AddressComponentTypePlaceEN), {
      example: Object.values(AddressComponentTypePlaceEN).join('/'),
    }),
    geoData: geoDataCreateReq,
  })
);
export type SAComponentPlaceUpdateReqDto = z.infer<typeof saComponentPlaceUpdateReq>;
