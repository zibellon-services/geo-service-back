import { extendApi } from '@anatine/zod-openapi';
import { AddressComponentZone } from 'utils/constants';
import { AddressComponentTypeEN } from 'utils/constants-en';
import { z } from 'zod';
import { geoDataCreateReq } from '../common/geo.dto';

//REQ
export const saComponentAddressGetListReq = extendApi(
  z.object({
    parentComponentId: extendApi(z.string().uuid().optional(), {
      example: '69e45d20-98a9-4c75-ab60-1188d64d510a',
    }),
    zone: extendApi(z.string().optional(), {
      example: 'SomeName',
    }),
    searchValue: extendApi(z.string().optional(), {
      example: 'SomeName',
    }),
  })
);
export type SAComponentAddressGetListReqDto = z.infer<typeof saComponentAddressGetListReq>;

export const saComponentAddressCreateReq = extendApi(
  z.object({
    parentComponentId: extendApi(z.string().uuid().nullable().optional(), {
      example: '69e45d20-98a9-4c75-ab60-1188d64d510a',
    }),
    name: extendApi(z.string().min(1), {
      example: 'SomeName',
    }),
    zone: extendApi(z.nativeEnum(AddressComponentZone), {
      example: Object.values(AddressComponentZone).join('/'),
    }),
    type: extendApi(z.nativeEnum(AddressComponentTypeEN), {
      example: Object.values(AddressComponentTypeEN).join('/'),
    }),
    geoData: geoDataCreateReq.optional(), //Почему optional ???
  })
);
export type SAComponentAddressCreateReqDto = z.infer<typeof saComponentAddressCreateReq>;

export const saComponentAddressUpdateReq = extendApi(
  z.object({
    componentId: extendApi(z.string().uuid(), {
      example: '69e45d20-98a9-4c75-ab60-1188d64d510a',
    }),
    name: extendApi(z.string().min(1), {
      example: 'SomeName',
    }),
    type: extendApi(z.nativeEnum(AddressComponentTypeEN), {
      example: Object.values(AddressComponentTypeEN).join('/'),
    }),
    geoData: geoDataCreateReq.optional(), //Почему optional ???
  })
);
export type SAComponentAddressUpdateReqDto = z.infer<typeof saComponentAddressUpdateReq>;

//RES
//...
