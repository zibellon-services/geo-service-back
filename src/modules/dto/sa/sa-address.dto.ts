import { extendApi } from '@anatine/zod-openapi';
import { z } from 'zod';
import { addressComponentDefaultRes } from '../default/address-component-default.dto';
import { addressDefaultRes } from '../default/address-default.dto';
import { geoDataDefaultRes } from '../default/geo-data-default.dto';

//REQ
export const saAddressGetListReq = extendApi(
  z.object({
    parentComponentId: extendApi(z.string().uuid().optional(), {
      example: '69e45d20-98a9-4c75-ab60-1188d64d510a',
    }),
    searchValue: extendApi(z.string().optional(), {
      example: 'SomeName',
    }),
  })
);
export type SAAddressGetListReqDto = z.infer<typeof saAddressGetListReq>;

//RES
export const saAddressListRes = z.object({
  addressList: addressDefaultRes
    .merge(
      z.object({
        area: addressComponentDefaultRes,
        areaDetails: addressComponentDefaultRes,
        locality: addressComponentDefaultRes,
        localityDetails: addressComponentDefaultRes,
        localityWay: addressComponentDefaultRes,
        house: addressComponentDefaultRes,
        houseDetails1: addressComponentDefaultRes,
        houseDetails2: addressComponentDefaultRes,
        place: addressComponentDefaultRes,
        targetComponent: addressComponentDefaultRes,
        geoData: geoDataDefaultRes,
      })
    )
    .array(),
});
