import { extendApi } from '@anatine/zod-openapi';
import { AddressComponentZone } from 'utils/constants';
import { AddressComponentTypeEN } from 'utils/constants-en';
import { AddressComponentTypeRU } from 'utils/constants-ru';
import { z } from 'zod';

//REQ
export const apiV1AutocompleteReq = extendApi(
  z.object({
    searchValue: extendApi(z.string().min(1), {
      example: 'Архангельск воскресенская 118',
    }),
    area_priority: extendApi(z.string().optional(), {
      example: '???',
    }),
    locality_priority: extendApi(z.string().optional(), {
      example: '???',
    }),
    locality_way_priority: extendApi(z.string().optional(), {
      example: '???',
    }),
  })
);
export type ApiV1AutocompleteReqDto = z.infer<typeof apiV1AutocompleteReq>;

export const apiV1RoutingReq = extendApi(
  z.object({
    coordsString: extendApi(z.string().optional(), {
      example: 'a1,b1;a2,b2;a3,b3',
    }),
    polylineString: extendApi(z.string().optional(), {
      example: '???',
    }),
  })
);
export type ApiV1RoutingReqDto = z.infer<typeof apiV1RoutingReq>;

export const apiV1GeocodeReverseReq = extendApi(
  z.object({
    lat: extendApi(z.coerce.number(), {
      example: 11.87,
    }),
    lng: extendApi(z.coerce.number(), {
      example: 23.56,
    }),
  })
);
export type ApiV1GeocodeReverseReqDto = z.infer<typeof apiV1GeocodeReverseReq>;

export const apiV1GeocodeSearchReq = extendApi(
  z.object({
    searchValue: extendApi(z.string().min(1), {
      example: 'Архангельск воскресенская 118',
    }),
    area_priority: extendApi(z.string().optional(), {
      example: '???',
    }),
    locality_priority: extendApi(z.string().optional(), {
      example: '???',
    }),
    locality_way_priority: extendApi(z.string().optional(), {
      example: '???',
    }),
  })
);
export type ApiV1GeocodeSearchReqDto = z.infer<typeof apiV1GeocodeSearchReq>;

//RES

//ADDRESS
export const apiV1AddressComponentRes = extendApi(
  z.object({
    name: extendApi(z.string(), {
      example: 'Название компонента',
    }),
    zone: extendApi(z.nativeEnum(AddressComponentZone), {
      example: Object.values(AddressComponentZone).join('/'),
    }),
    type: extendApi(z.nativeEnum(AddressComponentTypeRU), {
      example: Object.values(AddressComponentTypeRU).join('/'),
    }),
    typeKey: extendApi(z.nativeEnum(AddressComponentTypeEN), {
      example: Object.values(AddressComponentTypeEN).join('/'),
    }),
  })
);

export const apiV1AddressCoordinatesRes = extendApi(
  z.object({
    lat: extendApi(z.number(), {
      example: 64.5344277,
    }),
    lng: extendApi(z.number(), {
      example: 64.5344277,
    }),
  })
);

export const apiV1AddressRes = extendApi(
  z.object({
    osmId: extendApi(z.number().optional(), {
      example: 12345,
    }),
    osmType: extendApi(z.string().optional(), {
      example: 'way/node/relation',
    }),
    dbId: extendApi(z.string().uuid().optional(), {
      example: 'way/node/relation',
    }),
    displayName: extendApi(z.string(), {
      example: 'область Архангельская, город Архангельск, проспект Московский, дом 41',
    }),
    displayDescription: extendApi(z.string(), {
      example: 'область Архангельская, город Архангельск, проспект Московский, дом 41',
    }),
    componentList: apiV1AddressComponentRes.array(),
    type: extendApi(z.nativeEnum(AddressComponentTypeRU), {
      example: Object.values(AddressComponentTypeRU).join('/'),
    }),
    typeKey: extendApi(z.nativeEnum(AddressComponentTypeEN), {
      example: Object.values(AddressComponentTypeEN).join('/'),
    }),
    coordinates: apiV1AddressCoordinatesRes,
  })
);

export const apiV1AddressListRes = z.object({
  addressList: apiV1AddressRes.array(),
});

//ROUTES
export const apiV1RouteRes = extendApi(
  z.object({
    geometry: extendApi(z.string(), {
      example: 'geo-string-json',
    }),
    distance: extendApi(z.number(), {
      example: 12234.53,
    }),
    duration: extendApi(z.number(), {
      example: 333.42,
    }),
  })
);

export const apiV1RouteListRes = z.object({
  routeList: apiV1RouteRes.array(),
});
