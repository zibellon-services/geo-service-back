import { extendApi } from '@anatine/zod-openapi';
import { AddressType } from 'utils/constants';
import { z } from 'zod';

export const addressDefaultRes = extendApi(
  z.object({
    id: extendApi(z.string().uuid(), {
      example: '69e45d20-98a9-4c75-ab60-1188d64d510a',
    }),
    displayName: extendApi(z.string(), {
      example: 'Москва, ул. Стромынка 20',
    }),
    displayDescription: extendApi(z.string(), {
      example: 'Москва, ул. Стромынка 20',
    }),
    type: extendApi(z.nativeEnum(AddressType), {
      example: Object.values(AddressType).join('/'),
    }),
    searchArea: extendApi(z.string(), {
      example: 'searchArea',
    }),
    searchAreaDetails: extendApi(z.string(), {
      example: 'searchAreaDetails',
    }),
    searchLocality: extendApi(z.string(), {
      example: 'searchLocality',
    }),
    searchLocalityDetails: extendApi(z.string(), {
      example: 'searchLocalityDetails',
    }),
    searchLocalityWay: extendApi(z.string(), {
      example: 'searchLocalityWay',
    }),
    searchHouse: extendApi(z.string(), {
      example: 'searchHouse',
    }),
    searchHouseDetails1: extendApi(z.string(), {
      example: 'searchHouseDetails1',
    }),
    searchHouseDetails2: extendApi(z.string(), {
      example: 'searchHouseDetails2',
    }),
    searchPlace: extendApi(z.string(), {
      example: 'searchPlace',
    }),
    areaId: extendApi(z.string().uuid(), {
      example: '69e45d20-98a9-4c75-ab60-1188d64d510a',
    }),
    areaDetailsId: extendApi(z.string().uuid(), {
      example: '69e45d20-98a9-4c75-ab60-1188d64d510a',
    }),
    localityId: extendApi(z.string().uuid(), {
      example: '69e45d20-98a9-4c75-ab60-1188d64d510a',
    }),
    localityDetailsId: extendApi(z.string().uuid(), {
      example: '69e45d20-98a9-4c75-ab60-1188d64d510a',
    }),
    localityWayId: extendApi(z.string().uuid(), {
      example: '69e45d20-98a9-4c75-ab60-1188d64d510a',
    }),
    houseId: extendApi(z.string().uuid(), {
      example: '69e45d20-98a9-4c75-ab60-1188d64d510a',
    }),
    houseDetails1Id: extendApi(z.string().uuid(), {
      example: '69e45d20-98a9-4c75-ab60-1188d64d510a',
    }),
    houseDetails2Id: extendApi(z.string().uuid(), {
      example: '69e45d20-98a9-4c75-ab60-1188d64d510a',
    }),
    placeId: extendApi(z.string().uuid(), {
      example: '69e45d20-98a9-4c75-ab60-1188d64d510a',
    }),
    targetComponentId: extendApi(z.string().uuid(), {
      example: '69e45d20-98a9-4c75-ab60-1188d64d510a',
    }),
    geoDataId: extendApi(z.string().uuid(), {
      example: '69e45d20-98a9-4c75-ab60-1188d64d510a',
    }),
  })
);
