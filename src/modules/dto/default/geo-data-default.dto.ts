import { extendApi } from '@anatine/zod-openapi';
import { z } from 'zod';

export const geoDataPointDefaultRes = extendApi(
  z.object({
    crs: extendApi(z.object({}), {
      example: {},
    }),
    type: extendApi(z.string(), {
      example: 'Point',
    }),
    coordinates: extendApi(z.number().array(), {
      example: [64.549116, 40.5710353],
    }),
  })
);

export const geoDataPolygonDefaultRes = extendApi(
  z.object({
    crs: extendApi(z.object({}), {
      example: {},
    }),
    type: extendApi(z.string(), {
      example: 'Polygon',
    }),
    coordinates: extendApi(z.number().array().array().array(), {
      example: [[[64.54908907358657, 40.552618665146824]]],
    }),
  })
);

export const geoDataDefaultRes = extendApi(
  z.object({
    id: extendApi(z.string().uuid(), {
      example: '69e45d20-98a9-4c75-ab60-1188d64d510a',
    }),
    point: geoDataPointDefaultRes,
    polygon: geoDataPolygonDefaultRes,
    componentId: extendApi(z.string().uuid(), {
      example: '69e45d20-98a9-4c75-ab60-1188d64d510a',
    }),
  })
);

// export default class GeoDataModels {
//   //Request
//   static reqPoint = {
//     lat: 64.54908907358657,
//     lng: 64.54908907358657,
//   };

//   static reqPolygon = {
//     polygon: [GeoDataModels.reqPoint],
//   };

//   static reqGeoData = {
//     point: GeoDataModels.reqPoint,
//     polygon: GeoDataModels.reqPolygon,
//   };
// }
