import { extendApi } from '@anatine/zod-openapi';
import { AddressComponentZone } from 'utils/constants';
import { AddressComponentTypeEN } from 'utils/constants-en';
import { z } from 'zod';

export const addressComponentDefaultRes = extendApi(
  z.object({
    id: extendApi(z.string().uuid(), {
      example: '69e45d20-98a9-4c75-ab60-1188d64d510a',
    }),
    name: extendApi(z.string(), {
      example: 'Петровка',
    }),
    zone: extendApi(z.nativeEnum(AddressComponentZone), {
      example: Object.values(AddressComponentZone).join('/'),
    }),
    type: extendApi(z.nativeEnum(AddressComponentTypeEN), {
      example: Object.values(AddressComponentTypeEN).join('/'),
    }),
    parentId: extendApi(z.string().uuid(), {
      example: '69e45d20-98a9-4c75-ab60-1188d64d510a',
    }),
  })
);
