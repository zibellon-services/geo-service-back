import { ApiController, Get } from 'core/api-decorators';
import { queryValidator } from 'core/middlewares/validators/query-validator';
import { saAddressGetListReq, saAddressListRes } from 'modules/dto/sa/sa-address.dto';
import { SAAddressService } from 'modules/services/sa/sa-address.service';
import { swBody200 } from 'utils/utils-swagger';

@ApiController('/api/v1/sa/address')
@Get({
  path: '/',
  summary: 'Получение всех адресов',
  dtoQuerySchema: saAddressGetListReq,
  handlers: [queryValidator],
  responseList: [swBody200(saAddressListRes)],
  func: async (ctx) => {
    return await SAAddressService.getAll(ctx.dtoQuery);
  },
})
export class SAAddressController {}
