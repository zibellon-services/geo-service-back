import { ApiController, Delete, Get, Patch, Post } from 'core/api-decorators';
import { bodyValidator } from 'core/middlewares/validators/body-validator';
import { queryValidator } from 'core/middlewares/validators/query-validator';
import { saComponentGetListRes, saComponentGetOneRes, saComponentIdReqRes } from 'modules/dto/common/sa.dto';
import {
  saComponentPlaceCreateReq,
  saComponentPlaceGetListReq,
  saComponentPlaceUpdateReq,
} from 'modules/dto/sa/sa-component-place.dto';
import { SAComponentPlaceService } from 'modules/services/sa/sa-component-place.service';
import { swBody200, swMessage200 } from 'utils/utils-swagger';

@ApiController('/api/v1/sa/component/place')
@Get({
  path: '/',
  summary: 'Получить список всех мест',
  dtoQuerySchema: saComponentPlaceGetListReq,
  handlers: [queryValidator],
  responseList: [swBody200(saComponentGetListRes)],
  func: async (ctx) => {
    return await SAComponentPlaceService.getAll(ctx.dtoQuery);
  },
})
@Get({
  path: '/one',
  summary: 'Получение места по ID',
  dtoQuerySchema: saComponentIdReqRes,
  handlers: [queryValidator],
  responseList: [swBody200(saComponentGetOneRes)],
  func: async (ctx) => {
    return await SAComponentPlaceService.getById(ctx.dtoQuery);
  },
})
@Post({
  path: '/',
  summary: 'Создание Place по ID',
  dtoBodySchema: saComponentPlaceCreateReq,
  handlers: [bodyValidator],
  responseList: [swBody200(saComponentGetOneRes)],
  func: async (ctx) => {
    return await SAComponentPlaceService.create(ctx.dtoBody);
  },
})
@Patch({
  path: '/',
  summary: 'Обновление Place по ID',
  dtoBodySchema: saComponentPlaceUpdateReq,
  handlers: [bodyValidator],
  responseList: [swBody200(saComponentGetOneRes)],
  func: async (ctx) => {
    return await SAComponentPlaceService.updateById(ctx.dtoBody);
  },
})
@Delete({
  path: '/one',
  summary: 'Удаление места по ID',
  dtoQuerySchema: saComponentIdReqRes,
  handlers: [queryValidator],
  responseList: [swMessage200()],
  func: async (ctx) => {
    return await SAComponentPlaceService.deleteById(ctx.dtoQuery);
  },
})
export class SAComponentPlaceController {}
