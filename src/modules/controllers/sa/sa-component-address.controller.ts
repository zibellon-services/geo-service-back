import { ApiController, Delete, Get, Patch, Post } from 'core/api-decorators';
import { bodyValidator } from 'core/middlewares/validators/body-validator';
import { queryValidator } from 'core/middlewares/validators/query-validator';
import { saComponentGetListRes, saComponentGetOneRes, saComponentIdReqRes } from 'modules/dto/common/sa.dto';
import {
  saComponentAddressCreateReq,
  saComponentAddressUpdateReq,
  saComponentAddressGetListReq,
} from 'modules/dto/sa/sa-component-address.dto';
import { SAComponentAddressService } from 'modules/services/sa/sa-component-address.service';
import { swBody200, swMessage200 } from 'utils/utils-swagger';

@ApiController('/api/v1/sa/component/address')
@Get({
  path: '/',
  summary: 'Получить все компоненты',
  dtoQuerySchema: saComponentAddressGetListReq,
  handlers: [queryValidator],
  responseList: [swBody200(saComponentGetListRes)],
  func: async (ctx) => {
    return await SAComponentAddressService.getAll(ctx.dtoQuery);
  },
})
@Get({
  path: '/one',
  summary: 'Получить компонент по ID',
  dtoQuerySchema: saComponentIdReqRes,
  handlers: [queryValidator],
  responseList: [swBody200(saComponentGetOneRes)],
  func: async (ctx) => {
    return await SAComponentAddressService.getById(ctx.dtoQuery);
  },
})
@Post({
  path: '/',
  summary: 'Создание компонента',
  dtoBodySchema: saComponentAddressCreateReq,
  handlers: [bodyValidator],
  responseList: [swBody200(saComponentGetOneRes)],
  func: async (ctx) => {
    return await SAComponentAddressService.create(ctx.dtoBody);
  },
})
@Patch({
  path: '/',
  summary: 'Обновление по ID',
  dtoBodySchema: saComponentAddressUpdateReq,
  handlers: [bodyValidator],
  responseList: [swBody200(saComponentGetOneRes)],
  func: async (ctx) => {
    return await SAComponentAddressService.updateById(ctx.dtoBody);
  },
})
@Delete({
  path: '/one',
  summary: 'Удаление по ID',
  dtoQuerySchema: saComponentIdReqRes,
  handlers: [queryValidator],
  responseList: [swMessage200()],
  func: async (ctx) => {
    return await SAComponentAddressService.deleteById(ctx.dtoQuery);
  },
})
export class SAComponentAddressController {}
