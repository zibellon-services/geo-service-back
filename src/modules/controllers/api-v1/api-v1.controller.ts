import { ApiController, Get } from 'core/api-decorators';
import { queryValidator } from 'core/middlewares/validators/query-validator';
import {
  apiV1AddressListRes,
  apiV1AddressRes,
  apiV1AutocompleteReq,
  apiV1GeocodeReverseReq,
  apiV1GeocodeSearchReq,
  apiV1RouteListRes,
  apiV1RoutingReq,
} from 'modules/dto/api-v1/api-v1.dto';
import { ApiV1Service } from 'modules/services/api-v1.service';
import { swBody200 } from 'utils/utils-swagger';

@ApiController('/api/v1')
@Get({
  path: '/autocomplete',
  summary: 'Получение списка адресов, по автодополнению',
  dtoQuerySchema: apiV1AutocompleteReq,
  handlers: [queryValidator],
  responseList: [swBody200(apiV1AddressListRes)],
  func: async (ctx) => {
    return await ApiV1Service.autocomplete(ctx.dtoQuery);
  },
})
@Get({
  path: '/routing',
  summary: 'Построение дороги по координатам',
  dtoQuerySchema: apiV1RoutingReq,
  handlers: [queryValidator],
  responseList: [swBody200(apiV1RouteListRes)],
  func: async (ctx) => {
    return await ApiV1Service.routing(ctx.dtoQuery);
  },
})
@Get({
  path: '/geocode/reverse',
  summary: 'reverse геокодирование. Координаты -> Адрес',
  dtoQuerySchema: apiV1GeocodeReverseReq,
  handlers: [queryValidator],
  responseList: [swBody200(apiV1AddressRes)],
  func: async (ctx) => {
    return await ApiV1Service.geocodeReverse(ctx.dtoQuery);
  },
})
@Get({
  path: '/geocode/search',
  summary: 'Прямое геокодирование. Адрес -> Координаты',
  dtoQuerySchema: apiV1GeocodeSearchReq,
  handlers: [queryValidator],
  responseList: [swBody200(apiV1AddressRes)],
  func: async (ctx) => {
    return await ApiV1Service.geocodeSearch(ctx.dtoQuery);
  },
})
export class APIV1Controller {}
