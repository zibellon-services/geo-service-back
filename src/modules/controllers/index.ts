import { APIV1Controller } from './api-v1/api-v1.controller';
import { SAAddressController } from './sa/sa-address.controller';
import { SAComponentAddressController } from './sa/sa-component-address.controller';
import { SAComponentPlaceController } from './sa/sa-component-place.controller';

export const controllers = [
  new APIV1Controller(),
  new SAAddressController(),
  new SAComponentAddressController(),
  new SAComponentPlaceController(),
];
