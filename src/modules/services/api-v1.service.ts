import { throwErrorSimple } from 'core/utils/error';
import { logInfo, logWarn } from 'core/utils/logger';
import { Address } from 'database/postgres/models/final/address.model';
import { addressIdListAutocompleteSQL } from 'database/postgres/sql/address-id-list-autocomplete';
import { addressIdReverseSQL } from 'database/postgres/sql/address-reverse';
import { addressSearchSQL } from 'database/postgres/sql/address-search';
import {
  ApiV1AutocompleteReqDto,
  ApiV1GeocodeReverseReqDto,
  ApiV1GeocodeSearchReqDto,
  ApiV1RoutingReqDto,
} from 'modules/dto/api-v1/api-v1.dto';
import { Op } from 'sequelize';
import { ResAPIV1Address } from 'utils/classes';
import { CONSTANTS, PHOTON_LANG_Q } from 'utils/constants';
import { escapeSQL, getOsmIdsFromPhotonArray, mapSpacesSearchValue } from 'utils/utils';
import { mapDBAddressToAPIV1Response } from 'utils/utils-map-db-address';
import { mapOSMAddressToAPIV1Response } from 'utils/utils-map-osm-address';
import { OSM_API, OSRM_API, PHOTON_API } from 'utils/utils-network';
import { sortResAPIV1AddressList } from 'utils/utils-sort';

export const ApiV1Service = {
  autocomplete: async (dtoQuery: ApiV1AutocompleteReqDto) => {
    //Получение списка адресов, по автодополнению
    //---
    const searchValue = escapeSQL(dtoQuery.searchValue);
    if (!searchValue.length) {
      throwErrorSimple(`Неверное значение для searchValue ${dtoQuery.searchValue}`);
    }
    let tmpAddressList: ResAPIV1Address[] = [];

    //Максимум - 10 ID
    //Архангельск, шлепы 149 -> Архангельск & шлепы & 149:*
    const searchValueWithoutSpaces = mapSpacesSearchValue(searchValue);
    const splittedSearch = searchValueWithoutSpaces.split(' ').filter((el) => el !== '');

    const addressIdList = await addressIdListAutocompleteSQL(
      splittedSearch.join(' & ').concat(':*'),
      splittedSearch[splittedSearch.length - 1].concat(':*')
    );

    //Если у нас в БД нашлось что-то больше 0
    if (addressIdList.length > 0) {
      const addressDBList = await Address.findAll({
        where: {
          id: {
            [Op.in]: addressIdList.map((el) => el.addressId),
          },
        },
        include: {
          all: true,
        },
      });
      tmpAddressList.push(...addressDBList.map(mapDBAddressToAPIV1Response));
    }

    //В БД нашлось МЕНЕЕ X адресов
    if (addressIdList.length < CONSTANTS.ADDRESS_AUTOCOMPLETE_LIMIT) {
      const responsePhoton = await PHOTON_API.get({
        url: '/api',
        params: {
          q: searchValue,
          limit: CONSTANTS.ADDRESS_AUTOCOMPLETE_LIMIT - addressIdList.length,
          osm_tag: ':!bus_stop',
          ...PHOTON_LANG_Q,
        },
      });
      if (!responsePhoton.isSuccess || !responsePhoton.data) {
        logWarn(`ApiV1Service.autocomplete.PHOTON_API.ERROR=${responsePhoton.message}`, responsePhoton);
        throwErrorSimple(responsePhoton.message ?? 'PHOTON_API.ERROR');
      }

      const osm_ids = getOsmIdsFromPhotonArray(responsePhoton.data.features);

      const responseOSM = await OSM_API.get<any[]>({
        url: '/lookup',
        params: {
          osm_ids,
          format: 'json',
          addressdetails: 1,
        },
      });
      if (!responseOSM.isSuccess || !responseOSM.data) {
        logWarn(`ApiV1Service.autocomplete.OSM_API.ERROR=${responseOSM.message}`, responseOSM);
        throwErrorSimple(responseOSM.message ?? 'OSM_API.ERROR');
      }

      const resultAddressList = responseOSM.data.map(mapOSMAddressToAPIV1Response).filter((el: any) => {
        return el.displayName.length > 0 && el.displayDescription.length > 0;
      });

      tmpAddressList.push(...resultAddressList);
    }

    //СОРТИРОВКА
    const addressList = sortResAPIV1AddressList(tmpAddressList, searchValue, {
      areaPriority: dtoQuery.area_priority,
      localityPriority: dtoQuery.locality_priority,
      localityWayPriority: dtoQuery.locality_way_priority,
    });

    return {
      addressList,
    };
  },
  routing: async (dtoQuery: ApiV1RoutingReqDto) => {
    //Построение дороги
    //---
    if (
      (!dtoQuery.coordsString || dtoQuery.coordsString.length === 0) &&
      (!dtoQuery.polylineString || dtoQuery.polylineString.length === 0)
    ) {
      throwErrorSimple('Неверные координаты');
    }

    let requestUrl = '/route/v1/driving';
    if (dtoQuery.coordsString && dtoQuery.coordsString.length > 0) {
      //Здесь проверка на то, что пришло хотябы 2 точки
      const splitCoords = dtoQuery.coordsString.split(';');
      if (splitCoords.length < 2) {
        throwErrorSimple('Неверные координаты');
      }
      requestUrl += `/${dtoQuery.coordsString}`;
    } else {
      requestUrl += `/polyline(${dtoQuery.polylineString})`;
    }

    const responseOSRM = await OSRM_API.get({
      url: requestUrl,
      params: {
        steps: 'false',
        overview: 'full',
        alternatives: 3,
      },
    });
    if (!responseOSRM.isSuccess || !responseOSRM.data) {
      logWarn(`ApiV1Service.routing.OSRM_API.ERROR=${responseOSRM.message}`, responseOSRM.errorData);
      throwErrorSimple(responseOSRM.message ?? 'OSRM_API.ERROR');
    }
    if (!responseOSRM.data.routes || responseOSRM.data.routes.length === 0 || !responseOSRM.data.routes[0].geometry) {
      throwErrorSimple('Не нашли ни одной дороги');
    }

    const routeList = responseOSRM.data.routes.map((el: any) => {
      return {
        geometry: el.geometry,
        distance: el.distance,
        duration: el.duration,
      };
    });

    return {
      routeList,
    };
  },
  geocodeReverse: async (dtoQuery: ApiV1GeocodeReverseReqDto): Promise<ResAPIV1Address> => {
    //reverse геокодирование. Координаты -> Адрес
    //---
    //Производим поиск в нашей БД по координатам
    const addressIdList = await addressIdReverseSQL(dtoQuery.lat, dtoQuery.lng);

    if (addressIdList.length > 0) {
      const address = await Address.findByPk(addressIdList[0].addressId, {
        include: {
          all: true,
        },
      });
      if (address) {
        return mapDBAddressToAPIV1Response(address);
      }
    }

    //Если не нашли - обращаемся к OSM
    const resReverse = await OSM_API.get({
      url: '/reverse',
      params: {
        lat: dtoQuery.lat,
        lon: dtoQuery.lng,
        format: 'json',
      },
    });
    if (!resReverse.isSuccess || !resReverse.data || !resReverse.data.address) {
      logWarn(`ApiV1Service.geocodeReverse.OSM_API.REVERSE.ERROR=${resReverse.message}`, resReverse);
      throwErrorSimple(resReverse.message ?? 'OSM_API.REVERSE.ERROR');
    }

    const osmType = String(resReverse.data.osm_type);
    const osmId = String(resReverse.data.osm_id);
    const osm_ids = osmType.charAt(0).toUpperCase() + osmId;

    const resLookup = await OSM_API.get<any[]>({
      url: '/lookup',
      params: {
        osm_ids,
        format: 'json',
        addressdetails: 1,
      },
    });
    if (!resLookup.isSuccess || !resLookup.data || resLookup.data.length === 0) {
      logWarn(`ApiV1Service.geocodeReverse.OSM_API.LOOKUP.ERROR=${resLookup.message}`, resLookup);
      throwErrorSimple(resLookup.message ?? 'OSM_API.LOOKUP.ERROR');
    }

    const resultAddressList = resLookup.data.map(mapOSMAddressToAPIV1Response).filter((el: any) => {
      return el.displayName.length > 0 && el.displayDescription.length > 0;
    });
    if (resultAddressList.length === 0) {
      throwErrorSimple('Не нашли ни одного подходящего адреса');
    }

    //Если НЕ нашли в OSM - выброс ошибки
    //Оно не прийдет пустым, будет округ / страна ...
    //Запись ЛОГА

    return resultAddressList[0];
  },
  geocodeSearch: async (dtoQuery: ApiV1GeocodeSearchReqDto): Promise<ResAPIV1Address> => {
    //Прямое геокодирование. Адрес -> Координаты
    //---
    //город Архангельск, улица Воскресенская, дом 116, строение 1
    //город Архангельск, ТРК "Титан-Арена" ??? ....
    //ТРК "Титан-Арена" ??? ....
    const searchValue = escapeSQL(dtoQuery.searchValue);
    if (!searchValue.length) {
      throwErrorSimple(`Неверное значение`);
    }
    const searchValueWithoutSpaces = mapSpacesSearchValue(searchValue);

    //Начинаем поиск в нашей БД

    const sqlSearchString = searchValueWithoutSpaces
      .split(' ')
      .filter((el) => el !== '')
      .join(' & ');

    //Максимум - 1 ID
    const addressDBIdList = await addressSearchSQL(sqlSearchString, CONSTANTS.ADDRESS_SEARCH_LIMIT);

    logInfo('ADDRESS FROM DB =', addressDBIdList);

    //Если у нас в БД нашлось что-то больше 0
    if (addressDBIdList.length > 0) {
      const addressFromDB = await Address.findByPk(addressDBIdList[0].addressId, {
        include: { all: true },
      });
      if (!addressFromDB) {
        throwErrorSimple('AddressNotFound');
      }
      return mapDBAddressToAPIV1Response(addressFromDB);
    }

    const responsePhoton = await PHOTON_API.get({
      url: '/api',
      params: {
        q: searchValueWithoutSpaces,
        limit: CONSTANTS.ADDRESS_SEARCH_LIMIT,
        osm_tag: ':!bus_stop',
        ...PHOTON_LANG_Q,
      },
    });
    //responsePhoton.data.features === any[], Если пустой массив - ошибка
    if (!responsePhoton.isSuccess || !responsePhoton.data || responsePhoton.data.features.length === 0) {
      logWarn(`ApiV1Service.geocodeSearch.PHOTON_API.api.ERROR=${responsePhoton.message}`, responsePhoton);
      throwErrorSimple(responsePhoton.message ?? 'PHOTON_API.api.ERROR');
    }

    const osm_ids = getOsmIdsFromPhotonArray(responsePhoton.data.features);

    const responseOSM = await OSM_API.get<any[]>({
      url: '/lookup',
      params: {
        osm_ids,
        format: 'json',
        addressdetails: 1,
      },
    });
    if (!responseOSM.isSuccess || !responseOSM.data || responseOSM.data.length === 0) {
      //пустой массив
      logWarn(`ApiV1Service.geocodeSearch.OSM_API.lookup.ERROR=${responseOSM.message}`, responseOSM);
      throwErrorSimple(responseOSM.message ?? 'OSM_API.lookup.ERROR');
    }

    const resultAddressList = responseOSM.data.map(mapOSMAddressToAPIV1Response).filter((el: any) => {
      return el.displayName.length > 0 && el.displayDescription.length > 0;
    });

    if (resultAddressList.length === 0) {
      logWarn(`ApiV1Service.geocodeSearch.resultAddressList.EMPTY`);
      throwErrorSimple('Не нашли ни одного подходящего адреса');
    }

    const addressList = sortResAPIV1AddressList(resultAddressList, searchValue, {
      areaPriority: dtoQuery.area_priority,
      localityPriority: dtoQuery.locality_priority,
      localityWayPriority: dtoQuery.locality_way_priority,
    });

    return addressList[0];
  },
};
