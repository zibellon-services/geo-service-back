import { throwErrorSimple } from 'core/utils/error';
import { Address } from 'database/postgres/models/final/address.model';
import { addressIdListAutocompleteSQL } from 'database/postgres/sql/address-id-list-autocomplete';
import { SAAddressGetListReqDto } from 'modules/dto/sa/sa-address.dto';
import { Op } from 'sequelize';
import { escapeSQL, mapSpacesSearchValue } from 'utils/utils';

export const SAAddressService = {
  getAll: async (dtoQuery: SAAddressGetListReqDto) => {
    const addressList: Address[] = [];

    if (!dtoQuery.searchValue) {
      const tmpList = await Address.findAll({
        include: { all: true },
      });
      addressList.push(...tmpList);
    } else {
      const searchValue = escapeSQL(dtoQuery.searchValue.trim());
      if (!searchValue.length) {
        throwErrorSimple(`Неверное значение`);
      }

      const searchValueWithoutSpaces = mapSpacesSearchValue(searchValue);

      const splittedSearch = searchValueWithoutSpaces.split(' ').filter((el) => el !== '');

      const addressIdList = await addressIdListAutocompleteSQL(
        splittedSearch.join(' & ').concat(':*'),
        splittedSearch[splittedSearch.length - 1].concat(':*')
      );

      const tmpList = await Address.findAll({
        where: {
          id: {
            [Op.in]: addressIdList.map((el) => el.addressId),
          },
        },
        include: { all: true },
      });

      addressList.push(...tmpList);
    }

    return {
      addressList,
    };
  },
};
