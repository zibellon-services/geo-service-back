import { throwErrorNotFound, throwErrorSimple } from 'core/utils/error';
import { AddressComponent } from 'database/postgres/models/final/address-component.model';
import { Address } from 'database/postgres/models/final/address.model';
import { GeoData } from 'database/postgres/models/final/geo-data.model';
import { SAComponentIdReqResDto } from 'modules/dto/common/sa.dto';
import {
  SAComponentAddressCreateReqDto,
  SAComponentAddressGetListReqDto,
  SAComponentAddressUpdateReqDto,
} from 'modules/dto/sa/sa-component-address.dto';
import { Includeable, Op, WhereOptions } from 'sequelize';
import { AddressComponentZone } from 'utils/constants';
import { getZoneIndex } from 'utils/utils';
import {
  addressComponentAfterUpdateTransaction,
  createAddressForComponentTransaction,
  updateAddressGeoDataByComponentTransaction,
} from 'utils/utils-address-component';
import { dbGetTrx, dbRunTrx } from 'utils/utils-db';
import { findEqualGeoDataSimple, mapGeoDataToCreateObj } from 'utils/utils-geodata';

const componentInclude: Includeable[] = [
  {
    model: GeoData,
    duplicating: false,
  },
  {
    model: Address,
    duplicating: false,
    include: [
      {
        all: true,
      },
    ],
  },
];

export const SAComponentAddressService = {
  getAll: async (dtoQuery: SAComponentAddressGetListReqDto) => {
    const where: WhereOptions = {
      zone: {
        [Op.ne]: AddressComponentZone.PLACE,
      },
      ...(dtoQuery.searchValue
        ? {
            name: {
              [Op.iLike]: `%${dtoQuery.searchValue}%`,
            },
          }
        : {}),
      ...(dtoQuery.parentComponentId
        ? {
            parentId: dtoQuery.parentComponentId,
          }
        : {}),
      ...(dtoQuery.zone
        ? {
            zone: dtoQuery.zone,
          }
        : {}),
    };

    const componentAddressList = await AddressComponent.findAll({
      where,
      include: componentInclude,
    });

    return {
      componentAddressList,
    };
  },
  getById: async (dtoQuery: SAComponentIdReqResDto) => {
    const addressComponent = await AddressComponent.findByPk(dtoQuery.componentId, {
      include: componentInclude,
    });
    if (!addressComponent) {
      throwErrorNotFound('AddressComponent not found');
    }
    return addressComponent;
  },
  create: async (dtoBody: SAComponentAddressCreateReqDto) => {
    let componentCreateObj: Record<string, string> = {};

    //Пришел родитель
    if (dtoBody.parentComponentId) {
      const parentComponent = await AddressComponent.findByPk(dtoBody.parentComponentId);

      //Проверка - что родительский компонент есть
      if (!parentComponent) {
        throwErrorNotFound(`Не нашли компонент по parentComponentId=${dtoBody.parentComponentId}`);
      }

      //Индекс РОДИТЕЛЯ должен быть МЕНЬШЕ индекса ребенка
      if (getZoneIndex(parentComponent.zone) >= getZoneIndex(dtoBody.zone)) {
        throwErrorSimple('Индекс зоны создаваемого компонента МЕНЬШЕ зоны родителя');
      }

      //Получить зону по ТИПУ....
      //Хз нужно ли ???
      componentCreateObj['parentId'] = dtoBody.parentComponentId;
    }

    //Если мы хотим создать GeoData
    //На всякий случай. Создать два адреса с ОДИНАКОВЫМ полигоном - нельзя
    if (dtoBody.geoData) {
      const geoDataFind = await findEqualGeoDataSimple(dtoBody.geoData);
      if (geoDataFind) {
        throwErrorSimple('Такой полигон уже занят');
      }
    }

    componentCreateObj = {
      ...componentCreateObj,
      name: dtoBody.name,
      zone: dtoBody.zone,
      type: dtoBody.type,
    };

    const transaction = await dbGetTrx();

    const addressComponent = await dbRunTrx(
      AddressComponent.create(
        {
          ...componentCreateObj,
        },
        {
          transaction,
        }
      ),
      transaction
    );

    //Если пришла GeoData -> создаем GeoData
    if (dtoBody.geoData) {
      const geoDataDB = await dbRunTrx(
        GeoData.create(
          {
            componentId: addressComponent.id,
            ...mapGeoDataToCreateObj(dtoBody.geoData),
          },
          {
            transaction,
          }
        ),
        transaction
      );
      addressComponent.geoData = geoDataDB;
    }

    //Создаем сам АДРЕС для этого компонента
    await createAddressForComponentTransaction(addressComponent, transaction);

    await transaction.commit();

    return await SAComponentAddressService.getById({
      componentId: addressComponent.id,
    });
  },
  updateById: async (dtoBody: SAComponentAddressUpdateReqDto) => {
    const addressComponent = await SAComponentAddressService.getById({
      componentId: dtoBody.componentId,
    });

    const transaction = await dbGetTrx();

    //Если с фронта пришли геоданные
    if (dtoBody.geoData) {
      //Ищем такие координаты в БД
      const geoDataFind = await findEqualGeoDataSimple(dtoBody.geoData);
      if (!geoDataFind) {
        //Не нашли
        //У компонента уже были геоданные - обновляем
        if (addressComponent.geoData) {
          await dbRunTrx(
            GeoData.update(
              {
                ...mapGeoDataToCreateObj(dtoBody.geoData),
              },
              {
                where: {
                  id: addressComponent.geoData.id,
                },
              }
            ),
            transaction
          );
        } else {
          //У компонента НЕ было геоданных - СОЗДАЕМ
          const geoDataDB = await dbRunTrx(
            GeoData.create(
              {
                componentId: addressComponent.id,
                ...mapGeoDataToCreateObj(dtoBody.geoData),
              },
              {
                transaction,
              }
            ),
            transaction
          );
          //Так как у компонента НЕ БЫЛО геоданных -> не было адреса
          await updateAddressGeoDataByComponentTransaction(geoDataDB, transaction);
        }
      }
    } else {
      //GeoData - не пришла с фронта (Оставили поле пустым -> удаляем)
      if (addressComponent.geoData) {
        await dbRunTrx(
          GeoData.destroy({
            where: {
              id: addressComponent.geoData.id,
            },
            transaction,
          }),
          transaction
        );
      }
    }

    await dbRunTrx(
      AddressComponent.update(
        {
          name: dtoBody.name,
          type: dtoBody.type,
        },
        {
          where: {
            id: addressComponent.id,
          },
        }
      ),
      transaction
    );

    await addressComponentAfterUpdateTransaction(addressComponent, transaction);

    await transaction.commit();

    return await SAComponentAddressService.getById({
      componentId: dtoBody.componentId,
    });
  },
  deleteById: async (dtoQuery: SAComponentIdReqResDto) => {
    const addressComponent = await AddressComponent.findByPk(dtoQuery.componentId);
    if (!addressComponent) {
      throwErrorNotFound(`Не нашли компонент по ID=${dtoQuery.componentId}`);
    }

    await addressComponent.destroy();

    return {
      message: 'Ok',
    };
  },
};
