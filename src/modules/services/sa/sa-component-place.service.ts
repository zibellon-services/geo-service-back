import { throwErrorNotFound, throwErrorSimple } from 'core/utils/error';
import { AddressComponent } from 'database/postgres/models/final/address-component.model';
import { Address } from 'database/postgres/models/final/address.model';
import { GeoData } from 'database/postgres/models/final/geo-data.model';
import { SAComponentIdReqResDto } from 'modules/dto/common/sa.dto';
import {
  SAComponentPlaceCreateReqDto,
  SAComponentPlaceGetListReqDto,
  SAComponentPlaceUpdateReqDto,
} from 'modules/dto/sa/sa-component-place.dto';
import { Includeable, Op, WhereOptions } from 'sequelize';
import { AddressComponentZone } from 'utils/constants';
import { getZoneIndex } from 'utils/utils';
import {
  addressComponentAfterUpdateTransaction,
  createAddressForComponentTransaction,
} from 'utils/utils-address-component';
import { dbGetTrx, dbRunTrx } from 'utils/utils-db';
import { findEqualGeoDataPlace, mapGeoDataToCreateObj } from 'utils/utils-geodata';

const placeInclude: Includeable[] = [
  {
    model: GeoData,
    duplicating: false,
  },
  {
    model: Address,
    duplicating: false,
    include: [
      {
        all: true,
      },
    ],
  },
];

export const SAComponentPlaceService = {
  getAll: async (dtoQuery: SAComponentPlaceGetListReqDto) => {
    const where: WhereOptions = {
      zone: AddressComponentZone.PLACE,
      ...(dtoQuery.searchValue
        ? {
            name: {
              [Op.iLike]: `%${dtoQuery.searchValue}%`,
            },
          }
        : {}),
    };

    const componentPlaceList = await AddressComponent.findAll({
      where,
      include: placeInclude,
    });

    return {
      componentPlaceList,
    };
  },
  getById: async (dtoQuery: SAComponentIdReqResDto) => {
    const address = await AddressComponent.findByPk(dtoQuery.componentId, {
      include: placeInclude,
    });
    if (!address) {
      throwErrorNotFound(`Не нашли компонент по ID=${dtoQuery.componentId}`);
    }
    return address;
  },
  create: async (dto: SAComponentPlaceCreateReqDto) => {
    const parentComponent = await AddressComponent.findByPk(dto.parentComponentId);

    //Проверка - что родительский компонент есть
    if (!parentComponent) {
      throwErrorNotFound(dto.parentComponentId);
    }

    //Индекс РОДИТЕЛЯ должен быть МЕНЬШЕ индекса ребенка
    if (getZoneIndex(parentComponent.zone) >= getZoneIndex(AddressComponentZone.PLACE)) {
      throwErrorSimple('Индекс зоны создаваемого компонента МЕНЬШЕ зоны родителя');
    }

    //Ищем полигон в БД - который привязан к компоненту типа = PLACE
    const geoDataFindPlace = await findEqualGeoDataPlace(dto.geoData);
    if (geoDataFindPlace) {
      throwErrorSimple('Для этого полигона уже есть место. Выберете другой полигон.');
    }

    const transaction = await dbGetTrx();

    //Создаем адрес с такими компонентами
    const placeComponent = await dbRunTrx(
      AddressComponent.create(
        {
          parentId: dto.parentComponentId,
          name: dto.name,
          zone: AddressComponentZone.PLACE,
          type: dto.type,
        },
        {
          transaction,
        }
      ),
      transaction
    );

    const geoDataDB = await dbRunTrx(
      GeoData.create(
        {
          componentId: placeComponent.id,
          ...mapGeoDataToCreateObj(dto.geoData),
        },
        {
          transaction,
        }
      ),
      transaction
    );
    placeComponent.geoData = geoDataDB;

    await createAddressForComponentTransaction(placeComponent, transaction);

    await transaction.commit();

    return await SAComponentPlaceService.getById({
      componentId: placeComponent.id,
    });
  },
  updateById: async (dtoBody: SAComponentPlaceUpdateReqDto) => {
    const placeComponent = await SAComponentPlaceService.getById({
      componentId: dtoBody.componentId,
    });

    const transaction = await dbGetTrx();

    //Ищем GeoData в БД по тем координатам, которые пришли с фронта
    //Ищем место, есть ли оно по таким координатам
    const geoDataFindPlace = await findEqualGeoDataPlace(dtoBody.geoData);

    if (geoDataFindPlace) {
      if (geoDataFindPlace.id !== placeComponent.id) {
        throwErrorSimple('Для этих координат уже есть ДРУГАЯ сущность PLACE');
      }
      //Это координаты нашего МЕСТА - ничего не делаем
    } else {
      //Еще нет места с такими координатами
      await dbRunTrx(
        GeoData.update(
          {
            ...mapGeoDataToCreateObj(dtoBody.geoData),
          },
          {
            where: {
              id: placeComponent.geoData.id,
            },
            transaction,
          }
        ),
        transaction
      );
    }

    //Обновляем место
    await dbRunTrx(
      AddressComponent.update(
        {
          name: dtoBody.name,
          type: dtoBody.type,
        },
        {
          where: {
            id: placeComponent.id,
          },
          transaction,
        }
      ),
      transaction
    );

    await addressComponentAfterUpdateTransaction(placeComponent, transaction);

    await transaction.commit();

    return await SAComponentPlaceService.getById({
      componentId: dtoBody.componentId,
    });
  },
  deleteById: async (dtoQuery: SAComponentIdReqResDto) => {
    const component = await AddressComponent.findByPk(dtoQuery.componentId);
    if (!component) {
      throwErrorNotFound(`Не нашли компонент по ID=${dtoQuery.componentId}`);
    }

    await AddressComponent.destroy({
      where: {
        id: dtoQuery.componentId,
      },
    });

    //МЕСТО - удалится КАСКАДОМ

    return {
      message: 'Ok',
    };
  },
};
